
'use strict';

const isDevel = true;
var app = angular.module('application', [
    'ui.router',
    'ngAnimate',
    'angularInlineEdit',
      'ngDialog',
    //foundation
    'foundation',
    'foundation.dynamicRouting',
    'foundation.dynamicRouting.animations'
  ]).constant('HTTP_CODES', {'UNAUTHORIZED': 401, 'PAYMENT_AUTHORISED' : 200 })
    .config(config)
    .run(run)
  ;

config.$inject = ['$urlRouterProvider', '$locationProvider'];

function config($urlProvider, $locationProvider) {
  $urlProvider.otherwise('/');

  $locationProvider.html5Mode({
    enabled:false,
    requireBase: false
  });

  $locationProvider.hashPrefix('!');
}

function run() {
  FastClick.attach(document.body);

}


app.factory('fbauthenticationProvider', [ 'oauth2Provider', function (oauth2Provider) {

  // var isDevel = true;
  var fbauthenticationProvider =  fbauthenticationProvider ||  {};

  fbauthenticationProvider.delegate = fbauthenticationProvider.delegate || {};
  fbauthenticationProvider.delegate.sendUser = function(){};

  fbauthenticationProvider.sendUser = function(sendUser) {
    fbauthenticationProvider.delegate.sendUser(sendUser)
  }


  fbauthenticationProvider.init = function (){

    if(isDevel)console.log('Now in init...');


    var APP_ID = '1803551136553931';

    var config = {
      apiKey: "AIzaSyBQ6cF6r_nNaxxrzCLIDtv-WHKFA5H2jl8",
      authDomain: "social-challenge-148216.firebaseapp.com",
      databaseURL: "https://social-challenge-148216.firebaseio.com",
      storageBucket: "social-challenge-148216.appspot.com",
      messagingSenderId: "51952109187"
    };
    firebase.initializeApp(config);


    FB.init({
      appId      : APP_ID,
      status     : true,
      xfbml      : true,
      version    : 'v2.8',
      cookie	 : true
    });
    FB.Event.subscribe('auth.authResponseChange', fbauthenticationProvider.checkLoginState);

    (function(d, s, id){
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {return;}
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));



    fbauthenticationProvider.initApp();


  };

  fbauthenticationProvider.checkLoginState = function(event) {

    if(isDevel)console.log('Now in Check Login State...');

    if (event.authResponse) {
      // User is signed-in Facebook.
      var unsubscribe = firebase.auth().onAuthStateChanged(function(firebaseUser) {
        unsubscribe();
        // Check if we are already signed-in Firebase with the correct user.
        if (!fbauthenticationProvider.isUserEqual(event.authResponse, firebaseUser)) {
          // Build Firebase credential with the Facebook auth token.
          // [START facebookcredential]
          var credential = firebase.auth.FacebookAuthProvider.credential(event.authResponse.accessToken);


          isDevel ? console.log("**** CREDENTIAL ****" + JSON.stringify( credential ) ) : 0;

          // [END facebookcredential]
          // Sign in with the credential from the Facebook user.
          // [START authwithcred]
          firebase.auth().signInWithCredential(credential).catch(function(error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            // The email of the user's account used.
            var email = error.email;
            // The firebase.auth.AuthCredential type that was used.
            var credential = error.credential;
            // [START_EXCLUDE]
            if (errorCode === 'auth/account-exists-with-different-credential') {
              alert('You have already signed up with a different auth provider for that email.');
              // If you are using multiple auth providers on your app you should handle linking
              // the user's accounts here.
            } else {
              console.error(error);
            }
            // [END_EXCLUDE]
          });
          // [END authwithcred]
        } else {
          // User is already signed-in Firebase with the correct user.
        }
      });
    } else {
      // User is signed-out of Facebook.
      // [START signout]
      firebase.auth().signOut();
      if(isDevel)console.log("Loggin out User from Firebase");
      // [END signout]
    }
  };
  // [END facebookcallback]
  /**
   * Check that the given Facebook user is equals to the  given Firebase user
   */
  // [START checksameuser]
  fbauthenticationProvider.isUserEqual =  function (facebookAuthResponse, firebaseUser) {
    if(isDevel)console.log('Now in isEqual...');

    if (firebaseUser) {
      var providerData = firebaseUser.providerData;
      for (var i = 0; i < providerData.length; i++) {
        if (providerData[i].providerId === firebase.auth.FacebookAuthProvider.PROVIDER_ID &&
          providerData[i].uid === facebookAuthResponse.userID) {
          // We don't need to re-auth the Firebase connection.
          return true;
        }
      }
    }
    return false;
  }
  // [END checksameuser]
  /**
   * initApp handles setting up UI event listeners and registering Firebase auth listeners:
   *  - firebase.auth().onAuthStateChanged: This listener is called when the user is signed in or
   *    out, and that is where we update the UI.
   */
  fbauthenticationProvider.initApp = function () {

    if(isDevel)console.log('Init Called...');


    // Listening for auth state changes.
    // [START authstatelistener]
    firebase.auth().onAuthStateChanged(function(user) {
      if(isDevel)console.log('Now in onAuthStateChanged...');
      if (user) {
        // User is signed in.
        var displayName = user.displayName;
        var email = user.email;
        var emailVerified = user.emailVerified;
        var photoURL = user.photoURL;
        var isAnonymous = user.isAnonymous;
        var uid = user.uid;
        var providerData = user.providerData;
        // [START_EXCLUDE]

        fbauthenticationProvider.sendUser(user);



        if(isDevel) console.log("CurrentUser " + JSON.stringify(user, null, '  '))

        if(!oauth2Provider.getFirebaseToken()) {

          user.getToken(true).then(function(idToken) {

            oauth2Provider.storeFirebaseToken(idToken)

          });

        }
        // [END_EXCLUDE]
      } else {
        // User is signed out.
          if(isDevel)console.log('No User or User has signed out ...');

    	  
        fbauthenticationProvider.user = null;
        oauth2Provider.clearAll();
        // [END_EXCLUDE]


      }
    });

  };

  fbauthenticationProvider.init();

  return fbauthenticationProvider;

}]);



app.factory('oauth2Provider', function () {


  var oauth2Provider = oauth2Provider || {};

  oauth2Provider.userSignedIn = false;
  oauth2Provider.fbToken =  "";
  oauth2Provider.firebaseToken = "";
  oauth2Provider.fbuser = "";
  oauth2Provider.stripeToken = "";

  const FACEBOOK_TOKEN = "facebookToken";
  const FIREBASE_TOKEN = "firebaseToken";
  const SIGNED_IN = "SignedIn";
  const FACEBOOK_USER_ID = 'fbuser';
  const CURRENT_USER = 'CURRENT_USER';
  const RESOLUTION = 'RESOLUTION';

  oauth2Provider.signedIn = function(signedIn) {


    if(isDevel)console.log("Setting Signed In " + signedIn);

    if (Modernizr.sessionstorage) {
      sessionStorage.setItem(SIGNED_IN, JSON.stringify({SIGNED_IN_KEY: signedIn}));
    }

    oauth2Provider.userSignedIn = signedIn;


  };

  oauth2Provider.isUserSignedIn = function() {

    if (Modernizr.sessionstorage) {

      var SignedIn = JSON.parse(sessionStorage.getItem(SIGNED_IN));

      if(SignedIn != null && SignedIn.SIGNED_IN_KEY != null) {
        if(isDevel)console.log("Is LOgged In " + SignedIn.SIGNED_IN_KEY);

        oauth2Provider.userSignedIn = SignedIn.SIGNED_IN_KEY;
      }
    }

    return oauth2Provider.userSignedIn;

  };


  oauth2Provider.clearAll = function() {

    if (Modernizr.sessionstorage) {
      sessionStorage.removeItem(FACEBOOK_TOKEN);
      sessionStorage.removeItem(FIREBASE_TOKEN);
      sessionStorage.removeItem(FACEBOOK_USER_ID);
      sessionStorage.removeItem(CURRENT_USER);
      sessionStorage.removeItem(RESOLUTION);

    }
    oauth2Provider.fbToken = "";
    oauth2Provider.firebaseToken = "";
    oauth2Provider.fbuser = "";
    oauth2Provider.fbuser = ""
  };


  oauth2Provider.removeFbToken = function() {
    if (Modernizr.sessionstorage) {
      sessionStorage.removeItem(FACEBOOK_TOKEN);
    }
    oauth2Provider.fbToken = "";
  };

  oauth2Provider.removeFirebaseToken = function() {

    if (Modernizr.sessionstorage) {
      sessionStorage.removeItem(FIREBASE_TOKEN);
    }
    oauth2Provider.firebaseToken = "";
  };


  oauth2Provider.getFbToken = function() {

    if (Modernizr.sessionstorage) {
      oauth2Provider.fbToken = sessionStorage.getItem(FACEBOOK_TOKEN);
    }
    return oauth2Provider.fbToken;

  };

  oauth2Provider.getFirebaseToken = function() {
    if (Modernizr.sessionstorage) {
      oauth2Provider.firebaseToken = sessionStorage.getItem(FIREBASE_TOKEN);
    }
    return  oauth2Provider.firebaseToken;
  };


  oauth2Provider.storeFbToken = function(token){

    if (Modernizr.sessionstorage) {

      sessionStorage.setItem(FACEBOOK_TOKEN, token);
      if(isDevel)console.log('FB Token in Session Storage.... ' +  sessionStorage.getItem(FACEBOOK_TOKEN));

    }
    // not-supported
    oauth2Provider.fbToken = token;

  };


  oauth2Provider.getFbUser = function() {

    if (Modernizr.sessionstorage) {
      oauth2Provider.fbuser = sessionStorage.getItem(FACEBOOK_USER_ID);
    }
    return oauth2Provider.fbuser;

  };


  oauth2Provider.storeFbUser = function(fbuser){

    if (Modernizr.sessionstorage) {

      sessionStorage.setItem(FACEBOOK_USER_ID, fbuser);
      if(isDevel)console.log('FB User Session Storage.... ' +  sessionStorage.getItem(FACEBOOK_USER_ID));

    }
    // not-supported
    oauth2Provider.fbuser = fbuser;

  };


  oauth2Provider.storeFirebaseToken = function(token) {

    if (Modernizr.sessionstorage) {
      sessionStorage.setItem(FIREBASE_TOKEN, token);

      if(isDevel)console.log('Firebase Token in Session Storage.... ' + sessionStorage.getItem(FIREBASE_TOKEN));
    }

    oauth2Provider.firebaseToken = token;

  };

  oauth2Provider.signIn = function (callBack) {


    FB.login(function(response) {

      if(isDevel) console.log("Response after login " + JSON.stringify(response));

      // Response is the TOKEN
      if(isDevel)console.log('Loggin in FBB...')

      if(response.authResponse == null) {
        callBack({loggedIn: false});
        return;
      }

      var token = response.authResponse.accessToken;
      var uid = response.authResponse.userID;

      if(token != null && uid != null) {
        oauth2Provider.storeFbToken(token);
        oauth2Provider.storeFbUser(uid)
      }else {
        oauth2Provider.storeFbToken("");
        oauth2Provider.storeFbUser("");

      }

    
      
      callBack({loggedIn: true});
      // handle the response
    }, {scope: 'publish_actions, email, user_friends, user_videos'});
//    }, {scope: 'publish_actions, user_friends, email, user_videos'});


  };

  oauth2Provider.signOut = function (callBack) {


    var fbToken = oauth2Provider.getFbToken();
    if(isDevel)console.log('Auth ' + fbToken);

    if(fbToken != null) {
      if(isDevel)console.log('Loggin out with Token... ' + fbToken)

      FB.logout(function(fbToken) {

        callBack({loggedIn: false});
        if(isDevel)console.log('Loggin out...')

      });
    }else {

// Somehow the FB token is missing
      callBack({loggedIn: false});

      if(isDevel)console.log('Loggin out coz there is no Auth Token...')

    }


  };


  oauth2Provider.videoPostForUploadedVideo = function(postObject, cb) {


    isDevel ? console.log("Message : " + "A very valuable message for " + postObject.tagged_literal ) : null
    isDevel ? console.log("link : " + postObject.permalink ) : null
    isDevel ? console.log("tags : " + postObject.tagged_ids ) : null

    FB.api(
      "/" + oauth2Provider.getFbUser() + "/feed",
      "POST",
      {
        "message": "A very valuable message for " + postObject.tagged_literal,
        "link": postObject.permalink,
        "tags": postObject.tagged_ids

      },
      function (response) {
        if (response && !response.error) {

          cb({"status": "success", "id": response.id});

        }else {

          cb({"status": "failure"});

        }
      }
    );
  };



///100014139613371/videos/156223378192315/

  oauth2Provider.videoSourceUrlForUploadedVideo = function(videoId, cb) {


    FB.api( "/" + videoId + "?fields=permalink_url", function(response) {

      if (response && !response.error) {

        if(isDevel)console.log("Source" + JSON.stringify(response));

        cb({source: response , error: false})

      }else {

        if(isDevel)console.log("Source  Error" + JSON.stringify(response.error));

        cb({source: "", error: true, errorMessage: response.error})

      }

    });


    /*    FB.api( "/" + videoId + "?fields=source",
     function(response) {

     if(isDevel)console.log("The Source Response " + JSON.stringify(response));

     var source = response.source;


     if (response && !response.error) {

     cb({"source": source , "error": true,  "errorMessage": response.error.message})

     }else {

     if(isDevel)console.log("Source  Error" + JSON.stringify(response.error.message));
     cb({"source": source, "error": false,  "errorMessage": response.error.message})

     }
     });*/

  };


  oauth2Provider.user_friends =  function(cb) {


    FB.api( "/" + oauth2Provider.getFbUser() + "/taggable_friends?fields=id,name&limit=1000",
      function (response) {
        if (response && !response.error) {

          if(isDevel)console.log("Taggable Successful");

          cb(response);



        }else {

          if(isDevel)console.log("Taggable  Error" + JSON.stringify(response.error));


          cb({success:false});
          // Error handling
        }
      }
    );

  };




  oauth2Provider.handler = function(callback) {


    return StripeCheckout.configure({
      key: 'pk_test_U9pNVS1sQ7vIdPsLps2Vs9qJ',
      // image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
      image: 'https://nyrchallenge.com/assets/img/apple-icon-120x120.png',
      locale: 'auto',
      token: function (token) {
        // You can access the token ID with `token.id`.
        // Get the token ID to your server-side code for use.

        if (token) {

          callback(token);
        }
      }
    });

  };




  return oauth2Provider;
});



/*
<form
enctype="multipart/form-data"
action="https://graph-video.facebook.com/148734798941173/videos?title=My%20Great%20Video&description=My%20Great%20description%20@[AaLMleZSBF2K2jZFKIBuLf8hVp9zkfOqYmwxPJfUuvuhJigYRFiMEjoz3odjERrPmHYb9YOOS48RxJs6bpu8ojtu2XD6XhKC0rvZviKxB4b5iA]&access_token=EAACEdEose0cBAO1irUlZBJtKvqelbi8DX3NnaNoLtZAt11aZA3vyWspg6EJD6P8pYGZAZC9HXPdZBmQGNR5zBFLVMyCiVUy0PoafA2kM8vy6I61hx8G7F8JZBpwLJO1HYQW48TcNrHFf6CkW576zRlVftoZAQdBKQCT0Wfs0ZCIflIwZDZD"
method="POST">

  <input type = "file" name = "file" />
  <input type = "submit" />
  </form
*/



app.controller('AboutUsCrlr', AboutUsCrlr);
AboutUsCrlr.$inject = ['$scope', '$stateParams', '$state', '$controller', 'oauth2Provider'];

function AboutUsCrlr($scope, $stateParams, $state, $controller, oauth2Provider) {

  angular.extend(this, $controller('DefaultController', {$scope: $scope, $stateParams: $stateParams, $state: $state}));

  $scope.name = "john";


  $scope.Video = function () {
  }

  $scope.init = function() {

    fileUpload("1803551136553931" , function(response){

      if(isDevel)console.log("Response " + JSON.stringify(response));


    });
  };


 var fileUpload = function (videoId, cb) {



      FB.api( "/" + videoId + "?fields=source", function(cb) {

        if (response && !response.error) {
          var source = response.source;
          cb({source: source , error: false})
        }else {
          if(isDevel)console.log("Source  Error" + JSON.stringify(response.error));
          cb({source: "", error: true, errorMessage: response.error})
        }
      });




  }
}

angular.module('application').controller('ContactController', ContactController);
ContactController.$inject = ['$scope', '$stateParams', '$state', '$controller'];

function ContactController($scope, $stateParams, $state, $controller) {

  angular.extend(this, $controller('DefaultController', {$scope: $scope, $stateParams: $stateParams, $state: $state}));


  $scope.name = "john";
}


app.controller('TasksController', TasksController);


TasksController.$inject = ['$scope', '$stateParams', '$state', '$controller', '$document', 'oauth2Provider' ];

// Thanks to LuminusDev for tip on extending DefaultController to preserve
// page animation functionality https://github.com/zurb/foundation-apps/issues/368
function TasksController($scope, $stateParams, $state, $controller, $document, oauth2Provider ) {

  angular.extend(this, $controller('DefaultController', {$scope: $scope, $stateParams: $stateParams, $state: $state}));




}



app.controller('CookieCrlr', CookieCrlr);
CookieCrlr.$inject = ['$scope', '$stateParams', '$state', '$controller', 'oauth2Provider'];

function CookieCrlr($scope, $stateParams, $state, $controller, oauth2Provider) {

  angular.extend(this, $controller('DefaultController', {$scope: $scope, $stateParams: $stateParams, $state: $state}));

  $scope.name = "john";

}




app.controller('MakeYourResolutionCtrl', MakeYourResolutionCtrl);

MakeYourResolutionCtrl.$inject = ['$scope', '$stateParams', '$state', '$controller','oauth2Provider', 'HTTP_CODES', 'fbauthenticationProvider', 'FoundationApi', 'ngDialog',  '$timeout'];

function MakeYourResolutionCtrl($scope, $stateParams, $state, $controller, oauth2Provider, HTTP_CODES, fbauthenticationProvider, FoundationApi, ngDialog, $timeout ) {

  angular.extend(this, $controller('DefaultController', {$scope: $scope, $stateParams: $stateParams, $state: $state}));


  var isDevelMode = false;
  // var handle = null;


  $scope.resolution = $scope.resolution || {};
  $scope.form = $scope.form || {};
  var resolutionAction = resolutionAction || {};


  // Form
  $scope.form.opt_out_of_donating = false;

  $scope.form.opt_in_gift_aid = false;
  $scope.form.this_is_my_money = false;
  $scope.form.this_is_not_lottery = false;

  $scope.form.title = "Mr";
  $scope.form.firstName = "";
  $scope.form.lastName = "";
  $scope.form.address = "";
  $scope.form.postcode = "";

  $scope.form.currency = '£';
  $scope.form.donation = '10.00';

  var formContents = function() {

    var contributor = {};

    contributor.optOut = $scope.form.opt_out_of_donating;
    contributor.optInGiftAid = $scope.form.opt_in_gift_aid;
    contributor.myOwnMoney = $scope.form.this_is_my_money;
    contributor.noGambling = $scope.form.this_is_not_lottery;

    contributor.title = $scope.form.title ? $scope.form.title : "none supplied";
    contributor.firstName = $scope.form.firstName ? $scope.form.firstName : "none supplied";
    contributor.lastName = $scope.form.lastName ? $scope.form.lastName : "none supplied";
    contributor.address = $scope.form.address ? $scope.form.address : "none supplied";
    contributor.postcode = $scope.form.postcode ? $scope.form.postcode : "none supplied";

    contributor.amountDonated =  $scope.form.donation ? $scope.form.donation : 0;


    return contributor;
  };





  // Constants

  const DEFAULT_PROFILE_IMAGE_URL = 'assets/img/profile.png';
  const SIGNED_OUT = "Signed Out";
  const SIGNED_IN = "Signed In";
  const DEFAULT_RESOLUTION_TEXT = "Lets make 2017 a year for giving and living, I am going to make a difference starting with me in the mirror, my New Year’s Resolution is ";
  const DEFAULT_PLEDGE_TEXT_START = "As part of my Resolution, I am going to help make 2017 a year of giving by donating";
  const DEFAULT_PLEDGE_TEXT_END = "to 'World Child Cancer' and 'Schools4Good'.";
  const DEFAULT_CHALLENGE_TEXT_START = "I challenge";
  const DEFAULT_CHALLENGE_TEXT_END = "to make a resolution and stick to it.";
  const RESOLUTION = 'RESOLUTION';
  const CURRENT_USER = 'CURRENT_USER';
  const CURRENT_FORM = 'CURRENT_FORM';

  const STATUS_PENDING = 'PENDING';
  const STATUS_SUCCESS = 'SUCCESS';


  $scope.loading = false;
  $scope.default_resolution_text = DEFAULT_RESOLUTION_TEXT;
  $scope.isPageDisabled = true;
  $scope.isUserLoggedIn = false;
  $scope.image_chosen = false;


  $scope.resolution.currentResolution = DEFAULT_RESOLUTION_TEXT;
  $scope.resolution.currentPledgeStart = DEFAULT_PLEDGE_TEXT_START;
  $scope.resolution.currentPledgeEnd = DEFAULT_PLEDGE_TEXT_END;

  $scope.resolution.currentChallegeStart = DEFAULT_CHALLENGE_TEXT_START;
  $scope.resolution.currentChallegeEnd = DEFAULT_CHALLENGE_TEXT_END;
  $scope.resolution.currency = "£";
  $scope.resolution.tagged = "" ;
  $scope.resolution.donation = "5.00";
  $scope.resolution.avitar = DEFAULT_PROFILE_IMAGE_URL;
  $scope.resolution.name = "";
  $scope.resolution.intro = "";





  var setUpPage = function() {

    var currentUser = JSON.parse(sessionStorage.getItem(CURRENT_USER));
    if (isDevel) console.log("CurrentUser in storage " + JSON.stringify(currentUser, null, '  '));
    var resolution = JSON.parse(sessionStorage.getItem(RESOLUTION));
    if (isDevel) console.log("Current Resolution in storage " + JSON.stringify(resolution, null, '  '));

    var form = JSON.parse(sessionStorage.getItem(CURRENT_FORM));

    if (isDevel) console.log("Current Form in storage " + JSON.stringify(form, null, '  '));


    if (currentUser) {
      $timeout(function() {
        $scope.$apply(function(response) {
          $scope.resolution.avitar = currentUser.photoURL;
          $scope.resolution.name = currentUser.displayName;
          $scope.resolution.intro = "Welcome " + currentUser.displayName + ", please begin... ";
          $scope.isUserLoggedIn = true;
          $scope.isPageDisabled = false;


        });
      });

    }

    if (resolution) {

      $timeout(function() {

        $scope.$apply(function(response) {
          $scope.resolution.currentResolution = resolution.currentResolution;
          $scope.resolution.currentPledge = resolution.currentPledge;
          $scope.resolution.tagged = resolution.tagged;
          $scope.resolution.currentChallege = resolution.currentChallege;
          $scope.resolution.donation = resolution.donation;
          $scope.resolution.currency = resolution.currency;
          $scope.resolution.avitar = resolution.avitar;
          $scope.resolution.name = resolution.name;
          $scope.resolution.intro = resolution.intro;

        });

      });
    }

    if (form) {
      $timeout(function () {
        $scope.$apply(function (response) {
          $scope.form.donation = form.donation;
          $scope.form.currency = form.currency;

        })
      })
    }



  };

  var handle = null;

  $scope.init = function () {

    $(document).ready(function(){
      document.getElementById('at_mention_input').addEventListener('blur', function (e) {
        if (isDevel) console.log("In the blur");
        updateChallengeFriendsForUI();
      });
    });
    setUpPage();

    if(!isDevelMode && oauth2Provider && !oauth2Provider.isUserSignedIn()) {
      var aDialog = ngDialog.open({
        template: './templates/modal-window.html',
        className: 'ngdialog-theme-default',
        showClose: true,
        position: top
      });
    }

    handle = oauth2Provider.handler(handler);


  };


  /*

   <button class="button" id="payment-button" ng-click="callForm()">Call Pay Form</button>


   $scope.callForm = function() {


   handle.open({
   name: 'ISPFA Limited',
   description: '2 widgets',
   zipCode: true,
   currency: 'gbp',
   amount: 2000
   });


   };

   */


  /*

   var aDialog = ngDialog.open({
   template: './templates/modal-window-payment.html',
   className: 'ngdialog-theme-plain',
   showClose: true,
   position: top,
   cache: false,
   width: '50%',
   controller: 'MakeYourResolutionCtrl'
   });

   aDialog.closePromise.then(function (data) {
   isDevel ? console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id) : null;
   });
   */

  /*
   $scope.processPayment = function() {

   isDevel ? console.log("INSIDE THE FORM") : 0;

   var $form = $('#payment-form');

   $form.submit(function(event) {

   // Disable the submit button to prevent repeated clicks:
   $form.find('.button').prop('disabled', true);

   // Request a token from Stripe:
   Stripe.card.createToken($form, stripeResponseHandler);

   isDevel ? console.log("INSIDE THE FORM 2") : 0;
   // Prevent the form from being submitted:
   return false;
   });


   };

   var stripeResponseHandler = function(status, response) {

   isDevel ? console.log("Status " + status) : 0;
   isDevel ? console.log("Response " + JSON.stringify(response) ) : 0;
   // Grab the form:
   var $form = $('#payment-form');

   if (response.error) { // Problem!

   $('.fa-warning').remove();

   // Show the errors on the form:
   $form.find('.payment-errors').prepend('<i class = "fa fa-fw fa-warning"></i>')
   $form.find('.payment-errors-warning').text(response.error.message);
   $form.find('.button').prop('disabled', false); // Re-enable submission

   } else { // Token was created!
   $('.fa-warning').remove();
   $('.payment-errors-warning').remove();

   // Get the token ID:
   var token = response.id;

   // Insert the token ID into the form so it gets submitted to the server:
   $form.append($('<input type="hidden" name="stripeToken">').val(token));

   // Submit the form:
   $form.get(0).submit();
   }

   };
   */



  var delegate = delegate || {};

  // delegate user from FirebaseService

  delegate.sendUser = function (sendUser) {


    if (isDevel) console.log("CurrentUser in Delegate " + JSON.stringify(sendUser, null, '  '));

    sessionStorage.setItem(CURRENT_USER, JSON.stringify(sendUser));

    if (isDevel) console.log("CurrentUser being stored " + JSON.stringify(sendUser, null, '  '));




    $timeout(function() {
      $scope.$apply(function (response) {

        $scope.resolution.avitar = sendUser.photoURL;
        $scope.resolution.name = sendUser.displayName;
        $scope.resolution.intro = "Welcome " + sendUser.displayName + ", please begin... ";
        $scope.isUserLoggedIn = true;
        $scope.isPageDisabled = false;

      });
    });

  };


  fbauthenticationProvider.delegate = delegate;

  if (isDevel) console.log('oauth2Provider.isUserSignedIn ' + oauth2Provider.isUserSignedIn());


  $scope.signIn = function () {

    $scope.loading = true;



    var retrieveProfileCallback = function () {

      if (ngDialog) ngDialog.close();

      if (isDevel) console.log('Sign In');

      oauth2Provider.signIn(function (response) {

        if (isDevel) console.log('isLoggedIn ' + response.loggedIn);

        oauth2Provider.signedIn(response.loggedIn);

        var isLoggedIn = oauth2Provider.isUserSignedIn();

        if (isDevel) console.log('isLoggedIn after ' + isLoggedIn);


        if (isLoggedIn) {


          if (isDevel) console.log('Inside Apply of signIn ' + isLoggedIn);

          oauth2Provider.user_friends(function (response) {
              if (response.success != null && response.success == false) {
                // Failed to get any friends
                if (isDevel) console.log('No Friends :-(');
              }
              var friends = new Map();
              for (var i = 0; i < response.data.length; i++) {
                var data = response.data;
                friends.set(data[i].name, data[i].id);
              }
              var tribute = getTribute(friends);
            });


          if (isDevel) console.log("User is signed in and now we are going to set IsUserLoggedIn and IsPageDisabled.");

          setPageDefaultsWhenSignedIn();

        } else {

          if (isDevel) console.log('Dialog');

          var aDialog = ngDialog.open({
            template: './templates/modal-window.html',
            className: 'ngdialog-theme-default',
            showClose: true,
            position: top
          });
        }

      });

    };


    if(oauth2Provider.isUserSignedIn()) {


      var handleSessionResponse = function(response) {


        if(response.authResponse) {

          FB.logout(function(response) {

            if(isDevel)console.log('Loggin out as we are already logged in...');

            retrieveProfileCallback();

          })

        }else {
          if(isDevel)console.log('We were not logged in but we had the IsUserSigned In...');
          retrieveProfileCallback();
        }
      };

      FB.getLoginStatus(handleSessionResponse);


    }else {
      retrieveProfileCallback();
    }


    //
    /*    FB.logout(function(response) {

     if (!response.authResponse) {
     return;
     }

     retrieveProfileCallback();
     if(isDevel)console.log('Loggin out as we are already logged in...')

     });
     */

    //




  };


  var setPageDefaultsWhenSignedIn = function() {

    $timeout(function() {
      $scope.$apply(function () {


        if (oauth2Provider.isUserSignedIn()) {
          $scope.isUserLoggedIn = true;
          $scope.isPageDisabled = false;
          $scope.loading = false;
        } else {
          $scope.loading = false;
          $scope.isUserLoggedIn = false;
          $scope.isPageDisabled = true;
        }


      });
    });
  };

  var setPageDefaultsWhenSignedOut = function() {

    const SIGNED_IN = "SignedIn";

    sessionStorage.removeItem(CURRENT_USER);
    sessionStorage.removeItem(RESOLUTION);
    sessionStorage.removeItem(SIGNED_IN);

    $timeout(function() {
      $scope.$apply(function () {

        $scope.resolution.avitar = DEFAULT_PROFILE_IMAGE_URL;
        $scope.resolution.name = "";
        $scope.resolution.intro = "";
        $scope.isUserLoggedIn = false;
        $scope.isPageDisabled = true;
        $scope.loading = false;
        $scope.image_chosen = false;


      });
    });

    $("#files").empty(); // .replaceWith($("#files").val('').clone(true));
    $('progress').attr('value', 0);
  };


  $scope.signOut = function () {

    if (isDevel) console.log('Sign Out');

    $scope.loading = true;




    oauth2Provider.signOut(function (response) {


      oauth2Provider.signedIn(response.loggedIn);


      if(!oauth2Provider.isUserSignedIn()) {

        if (isDevel) console.log('Inside Apply of signout ' + oauth2Provider.isUserSignedIn());
        if (isDevel) console.log("Signing Out ... ");

        setPageDefaultsWhenSignedOut();

      }else {
        // something went wrong... What can I do...



      }


    });

  };


  /*
   *
   */


  $scope.authorize = function () {

    var display_Name, e_mail, userIdToken;

    userIdToken = oauth2Provider.getFirebaseToken();
    $scope.loading = true;

    gapi.client.conference.queryResolution({
      firebaseToken: userIdToken
    }).execute(function (resp) {

      if (resp.error) {
        if (isDevel) console.log("Error" + JSON.stringify(resp, null, '  '))

      } else {
        if (isDevel) console.log("Success" + JSON.stringify(resp, null, '  '))
      }
      $scope.loading = false;

    });
    // [END authstatelistener]
  };

  $scope.checkForUser = function () {

    if (isDevel) console.log("CurrentUser in Check for User Token " + JSON.stringify(oauth2Provider.fbToken, null, '  '))


  };


  var getTribute = function(friends) {
    if(friends == null)return;
    var friendsArray = [];
    friends.forEach(function(_value, _key) {
      var obj = {};
      obj.key = _key;
      obj.value = _key;
      obj.id = _value;
      console.log(obj.key + " = " + obj.value + " " + obj.id);

      friendsArray.push(obj);
    });

    var tribute = new Tribute({
      menuContainer: document.getElementById('content'),

      values: friendsArray,

      selectTemplate: function (item) {
        if (this.range.isContentEditable(this.current.element)) {
          return '<span contenteditable="false"><a href="javascript:void(0)" target="_blank" title="' + item.original.id + '">' + item.original.value + '</a></span>';
        }
        return '@' + item.original.value;
      },
      noMatchTemplate: function (tribute) {
        return '<li>No match!</li>';
      }
    });

    tribute.attach(document.getElementById('at_mention_input'));

    document.getElementById('at_mention_input').addEventListener('tribute-replaced', function (e) {
      if(isDevel) console.log('Text replaced by Tribute!');
    });

    /*
     document.getElementById('at_mention_input').addEventListener('tribute-no-match', function (e) {
     var values = [{key: 'Cheese Tacos', value: 'Cheese Tacos', email: 'cheesetacos@zurb.com'}];
     tribute.appendCurrent(values);
     });
     */
    return tribute;

  };


  // Change this to the location of your server-side upload handler:
  //var url = window.location.hostname === 'blueimp.github.io' ?'//jquery-file-upload.appspot.com/' : 'server/php/',

  var fileinput = function () {
    return document.getElementById('fileupload').value;
  };

  var getFb_user_Id = function() {
    return oauth2Provider.getFbUser();
  };

  var getFb_token = function() {
    return oauth2Provider.getFbToken();
  };

  var getResolution = function() {
    return $scope.resolution.currentResolution ;
  };

  var getPledge = function() {
    var line = $scope.resolution.currentPledgeStart;
    line +=  (" " + $scope.form.currency);
    line += (" " + $scope.form.donation);
    line += (" " + $scope.resolution.currentPledgeEnd);

    return  line;
  };



  var getChallenge = function() {
    var line = $scope.resolution.currentChallegeStart;
    line += (" " + getTags() + " "); // this was to be a different format
    line += (" " +  $scope.resolution.currentChallegeEnd);
    return  line;
  };


  var getIds = function() {
    var friends = getChallengeFriends();
    if(isDevel)console.log("Get ID friends " + JSON.stringify( friends) );
    var tag_set= "";
    if(friends != null) {
      for(var i = 0; i <  friends.length; i++) {
        tag_set += "{'tag_uid':'" + friends[i] + "', 'x':'0','y':'0'}";
        // if((friends.length - i ) > 1 )tag_set += ", ";
        tag_set +=   ((friends.length - i ) > 1 ) ? ", " : "";
      }
    }
    if(isDevel)console.log("TAG IDS " + "[" + tag_set + "]" );
    return "[" + tag_set + "]";
  };



  var getTagsVideo = function() {
    var allTags  = getChallengeFriends();
    if(isDevel)console.log("All tags names " + JSON.stringify( allTags) );
    var tag_set = "";
    if(allTags != null) {
      for(var i = 0; i <  allTags.length; i++) {
        tag_set += allTags[i];
        tag_set +=   ((allTags.length - i ) > 1 ) ? "," : "";
      }
    }

    if(isDevel)console.log("Tag Set " + tag_set);
    return tag_set;
  };



  var getTags = function() {
    var allTags  = getChallengeFriendsName();
    if(isDevel)console.log("All tags names " + JSON.stringify( allTags) );
    var tag_set = "";
    if(allTags != null) {
      for(var i = 0; i <  allTags.length; i++) {
        /// tag_set += "@" + allTags[i] + " ";
        tag_set += allTags[i];
        tag_set +=   ((allTags.length - i ) > 1 ) ? ", " : "";
      }
    }

    if(isDevel)console.log("Tag Set " + tag_set);
    return tag_set;
  };


  var getChallengeFriendsName = function() {
    var friends = [];
    $( "#at_mention_input" ).find("span").find("a").each(function( index ) {
      if(isDevel)console.log("Title " +  index + ": " + $( this ).text() );
      // friends.push($( this ).text());
      friends.push($( this ).text());
    });
    if(isDevel)console.log("Get Challenge friends name " + JSON.stringify( friends) );
    return friends.length > 0 ? friends : null;
  };




  var getChallengeFriends = function() {

    var friends = [];

    $( "#at_mention_input" ).find("span").find("a").each(function( index ) {
      if(isDevel)console.log("Title " +  index + ": " + $( this ).attr('title') );
      // friends.push($( this ).text());
      friends.push($( this ).attr('title'));


    });

    if(isDevel)console.log("Get Challenge friends " + JSON.stringify( friends) );


    return friends.length > 0 ? friends : null;
  };


  var updateChallengeFriendsForUI = function() {

    var friends = "";

    $( "#at_mention_input" ).find("span").find("a").each(function( index ){
      if(isDevel) console.log("Friend " +  index + ": " + $( this ).text() );
      friends += $( this ).text() + ", ";
    });

    if(isDevel) console.log( "Friends " + friends );

    $timeout(function() {
      $scope.$apply(function () {
        $scope.resolution.tagged = friends + "";
      });
    });
  };




  const PROTOCOL = "https://";
  const VIDEO_DOMAIN = "graph-video.facebook.com";
  const PHOTO_DOMAIN = "graph.facebook.com";
  const PHOTO_END_POINT = "photos";
  const VIDEO_END_POINT = "videos";
  const IMG_CAPTION = "caption=";
  const VIDEO_DESCRIPTION = "description=";
  const ACCESS_TOKEN = "access_token=";
  const TAGS = "tags=";
  const SEPERATER = "/";
  const AMP = "&";
  const Q_MARK = "?";
  const NEW_LINE = "%0A";
  const FACE_BOOK_DOMAIN = "www.facebook.com";
  const MAX_FILE_SIZE = 9999999;
  const RETURN_LINK = 'https://nyrchallenge.com';

  // PAYMENTS

  const NAME_COMPANY = 'ISPFA Limited';
  const DESCRIPTION_TEXT = 'New Year\'s Resolution Challenge';

  var currency = function() {

    if($scope.form.currency === "£") {
      return 'gbp';
    }else {
      return 'usd';
    }
  };

  var donation = function() {
    return $scope.form.donation * 100;
  };


  var getVideoUrl = function () {

    var url = "";
    url += PROTOCOL;
    url += VIDEO_DOMAIN;
    url += SEPERATER;
    url += getFb_user_Id();
    url += SEPERATER;
    url += VIDEO_END_POINT;
    url += Q_MARK;
    url += VIDEO_DESCRIPTION;
    if(!$scope.form.opt_out_of_donating) {
      url += (getResolution() + NEW_LINE + NEW_LINE + getPledge() + NEW_LINE + NEW_LINE + getChallenge());
    }else {
      url += (getResolution() + NEW_LINE + NEW_LINE + getChallenge());
    }
    url += NEW_LINE + "Make your New Years\'s Resolution " + RETURN_LINK;
    url += AMP;
    url += ACCESS_TOKEN;
    url += getFb_token();

    if(isDevel) console.log('Video URL ' + url);

    return url;
  };


  var getPhotoUrl = function () {

    var url = "";
    url += PROTOCOL;
    url += PHOTO_DOMAIN;
    url += SEPERATER;
    url += getFb_user_Id();
    url += SEPERATER;
    url += PHOTO_END_POINT;
    url += Q_MARK;
    url += IMG_CAPTION;
    if(!$scope.form.opt_out_of_donating) {
      url += (getResolution() + NEW_LINE + NEW_LINE + getPledge() + NEW_LINE + NEW_LINE + getChallenge());
    }else {
      url += (getResolution() + NEW_LINE + NEW_LINE + getChallenge());
    }
    url += NEW_LINE + "Make your New Years\'s Resolution " + RETURN_LINK;
    url += AMP;
    url += TAGS + getIds() ;
    url += AMP;
    url += ACCESS_TOKEN;
    url += getFb_token();

    if(isDevel) console.log('Photo URL ' + url);

    return url;
  };





  /*
   const video_url = 'https://graph-video.facebook.com/140930506388269/videos?' +
   'title=My%20CHunked%20Video' +
   '&description=My%20Great%20video' +
   '&access_token=EAAZAoUfjUG8sBAApOAR6KkGkm7SSBXvvkA3OmYXxS4JuOCFfuEjYtvnorqEG9FJFW60oMOedYNMZAsdUOhOWcUrV1tBZC33sRtNKqlfA0BMFmVqG2ZB4cMmwmaMvoC3JG17kx0FojqeZCRlLGaBAxA6OXfkUjxO0ZD';

   */
  //const url = 'https://graph.facebook.com';

  //'https://graph.facebook.com/v2.8/140930506388269/photos';


  /*



   <form enctype="multipart/form-data" action="https://graph-video.facebook.com/140930506388269/videos" +
   "?title=My%20Great%20Video" +
   "&description=My%20Great%20video" +
   "&access_token=EAAZAoUfjUG8sBALmPymjKqIb4pMmjlulHt726wXgF02TbZA5i89tg16P5nzs2T5ZBZCaaVfswtNMnY2ogozHPmUbkX2fu6UZBnaDmiydx8qpMmXENSII9l97EEKJjXZBazhHxFjzsPG6uGTilmxUBaqTl1ZBfKtBRutIGNOi2RUlglHQaS0wHVM" method="POST">
   */
  var getUrl = function (fileType) {

    //var urlString = Q_MARK + TITLE + "My fabulous title" + AMP + DESCRIPTION + "My description of the day" + AMP + ACCESS_TOKEN + "EAAZAoUfjUG8sBALTaZCF50DfpG7l4cz82LcQ8t6PQYSNKJy6b2OOHuIJQZBZCZBL4pZAIqS3ky7D8Ovp8PlpKf2uBESATiVgycrBF9b4S6549cx1TlC4sJGJnBnthedPqpQOuVaHGJwoqMkIxFazyMakd4DDUjoypZBUkFN7WrZCgT5LvyUk8Xof";

    if (fileType.indexOf('video') != -1) {

      return getVideoUrl();

    }
    return getPhotoUrl();
  };



  resolutionAction.attemptCompletePurchase = function (postId, status, firebaseToken, stripeToken) {




    gapi.client.conference.attemptChargeOnCard({'postId':postId,'status': status,'firebaseToken': firebaseToken, 'stripeToken': stripeToken}).execute(function (resp) {


      isDevel ? console.log("Return RESPONSE FROM Boolean " + JSON.stringify(resp) ) : 0;


      if (resp.code) {

        isDevel ? console.log("Return Error Message " + resp.code ) : 0;

        FoundationApi.publish('main-notifications-warning', {
          title: 'Oops',
          content: 'Sorry there was a problem with your payment but your Challenge was successful!'
        });

      }else {
        FoundationApi.publish('main-notifications', {
          title: 'Thank you...',
          content: 'Your payment has been accepted and your Challenge is complete.'
        });

      }

    });


  }


  resolutionAction.chargeCard = function (paymentToken, callback) {

    var firebaseToken = oauth2Provider.getFirebaseToken();


    gapi.client.conference.createChargePaymentDetails(paymentToken).execute(function (resp) {


      callback(resp)

    });

  };



  var handler = function(token) {

    isDevel ? console.log("Token " + token.id) : null;
    isDevel ? console.log(JSON.stringify("Error " + token.error)) : null;

    if(token.error) {

      FoundationApi.publish('main-notifications-warning', {
        title: 'There was a problem',
        content: 'Sorry there was a problem with your card or details - you can choose to tick the box "I do not wish to donate" to continue with your post.'
      });


    }else {

      // Create a Record on server


      resolutionAction.stripeToken = token.id;
      var paymentToken = formContents();
      paymentToken.firebaseToken = oauth2Provider.getFirebaseToken();
      paymentToken.currency = currency();

      isDevel ? console.log(" PD " + JSON.stringify(paymentToken)) : 0;

      paymentToken.stripeToken = token.id;
      paymentToken.status = STATUS_PENDING;


      FoundationApi.publish('main-notifications', {
        title: 'Notification...',
        content: 'We are processing your payment and getting ready to load... Just hang on a little!'
      });


      resolutionAction.chargeCard(paymentToken, function(resp) {

        isDevel ? console.log("Value of Resp " + JSON.stringify(resp)) : 0;

        if (!resp.result) {

          //  var errorMessage = resp.result.reason || '';

          FoundationApi.publish('main-notifications-warning', {
            title: 'There was a problem',
            content: 'Sorry there was a problem with your card or details - you can choose to tick the box "I do not wish to donate" to continue with your post.'

          });

        }else {

          resolutionAction.token = token;
          resolutionAction.handleClick(resolutionAction.data);

        }

      });

    }
  };




  var windowWasClosed = function() {

    isDevel ? console.log("CLOSED") : 0;


    FoundationApi.publish('main-notifications-warning', {
      title: 'Payment Window Closed',
      content: 'You can also send your Challenge without any donation - just click "I do not wish to donate" and continue.'
    });

  };



  var uploadButton = $('<button/>').addClass('fb-login-submit').prop('disabled', true).text('Processing...').on('click', function () {

    var $this = $(this), data = $this.data();

    if(!getTags()) {

      FoundationApi.publish('main-notifications-warning', {
        title: 'Your Challenge',
        content: 'You must Challenge at least one person!'
      });
      return;
    }


    var handleClick = function(data){

      if(!data)return;

      if (data.originalFiles[0]) {

        $scope.resolution.fileType = data.originalFiles[0]['type'];

        $('#fileupload').fileupload(
          'option',
          {
            url: getUrl(data.originalFiles[0]['type']),
            // maxChunkSize: 10000000,
            singleFileUploads: true,
            sequentialUploads: false,
            multipart: true,
            type: 'POST',
            dataType: 'json'
          }
        );
      }

      $this.off('click').text('Abort').on('click', function () {
        $this.remove();
        data.abort();

        FoundationApi.publish('main-notifications-warning', {
          title: 'Cancelled Post',
          content: 'Please try again later!'
        });


        $("#files").empty(); // .replaceWith($("#files").val('').clone(true));
        $('progress').attr('value', 0);

        $timeout(function () {
          $scope.$apply(function () {
            $scope.loading = false;
            $scope.image_chosen = false;

          });
        });

      });

      data.submit().always(function () {
        $this.remove();
      });
    }


    if(!$scope.form.opt_out_of_donating) {

      isDevel ? console.log("Currency " + currency()) : 0;

      handle.open({
        name: NAME_COMPANY,
        description: DESCRIPTION_TEXT,
        zipCode: true,
        currency: currency(),
        amount: donation(),
        locale: 'auto',
        panelLabel: "Donate Now"
      });

    }else {
      handleClick(data);
    }


    resolutionAction.handleClick = handleClick;
    resolutionAction.data = data;
  });


  $('#fileupload').fileupload({
    url: null,
    acceptFileTypes: /(\.|\/)(3g2|3gp|3gpp|asf|avi|dat|divx|dv|f4v|flv|m2ts|m4v|mkv|mod|mov|mp4|mpe|mpeg|mpeg4|mpg|mts|nsv|ogm|ogv|qt|tod|ts|vob|wmv|gif|jpe?g|png)$/i,
    dataType: 'json',
    // maxFileSize: MAX_FILE_SIZE,
    autoUpload: false,
    singleFileUploads: true,
    sequentialUploads: false,
    multipart: true,
    type: 'POST',
    disableImageResize: /Android(?!.*Chrome)|Opera/
      .test(window.navigator.userAgent),
    previewMaxWidth: 425,
    previewMaxHeight: 600
  }).on('fileuploadadd', function (e, data) {

    $("#files").empty(); // .replaceWith($("#files").val('').clone(true));
    $('progress').attr('value', 0);


    $timeout(function() {
      $scope.$apply(function () {
        $scope.image_chosen = true;
      });
    });

    data.context = $('<div/>').appendTo('#files');
    data.context.addClass('fix-position');

    var node = $('<p/>').append(uploadButton.clone(true).data(data)); // Here we are adding a clone of the upload button

    node.appendTo(data.context);


  }).on('fileuploadprocessalways', function (e, data) {

    var index = data.index,
      file = data.files[index],
      node = $(data.context.children()[index]);


    if (file.preview) {
      node
        .prepend('<br>')
        .prepend(file.preview);
    }
    if (file.error) {
      node
        .append('<br>')
        .append($('<span class="text-danger"/>').text(file.error));
    }

    if (index + 1 === data.files.length) {
      data.context.find('button')
        .text('Send my Resolution to Facebook')
        .prop('disabled', !!data.files.error);
    }


  }).on('fileuploadprogressall', function (e, data) {

    $timeout(function() {
      $scope.$apply(function () {
        $scope.loading = true;
      });
    });

    var progress = parseInt(data.loaded / data.total * 100, 10);

    $('progress').attr('value', progress);


  }).on('fileuploaddone', function (e, data) {

    //  var response = JSON.stringify(data);

    var successFulFacebookUpload = false;
    var fileTypeOfUploadIsVideo =  ($scope.resolution.fileType.indexOf('video') != -1) ? true : false;
    var postId = "";
    var optedInToADonation = $scope.form.opt_out_of_donating;
    var firebaseToken = oauth2Provider.getFirebaseToken();

    if(isDevel)console.log("IN DONE");
    //if(isDevel)console.log(response);

    var result = eval("(" + JSON.stringify(data.result) + ")");
    if(isDevel)console.log("ID RETURN " +  result.id);

    postId = result.id;

    var message = 'You have successfully loaded your Photo/Video to Facebook - please logout to protect your account.';

    if(!optedInToADonation) {
      message = 'You have successfully loaded your Photo/Video to Facebook but we are still processing your upload - please wait.'
    }

    FoundationApi.publish('main-notifications', {
      title: 'Well done...',
      content: message
    });

    if(fileTypeOfUploadIsVideo) {

      // Video

      if(result && result.id) {

        oauth2Provider.videoSourceUrlForUploadedVideo(result.id, function(response) {

          if(response.error == false) {

            (isDevel) ? console.log("The Permalink URL of the video is " + JSON.stringify(response) ) : 0;

            //  send post with tags
            var postObject = {};
            postObject.tagged_literal = getTags() + ".";
            postObject.permalink = PROTOCOL + FACE_BOOK_DOMAIN + response.source.permalink_url;
            postObject.tagged_ids = getTagsVideo();

            oauth2Provider.videoPostForUploadedVideo(postObject, function(response) {

              if(response.status === "success") {

                isDevel ? console.log("ID = " + response.id) : null;

                postId = response.id;

              }else{

                FoundationApi.publish('main-notifications-warning', {
                  title: 'Oops...',
                  content: 'The facebook system is having a bad day - we did post to your video to your timeline but there was a problem sending out your challenge to your tagged friends. You can share you video from your timeline.'
                });

              }

              // Do we charge?
              if(!optedInToADonation) {
                resolutionAction.attemptCompletePurchase(postId, STATUS_SUCCESS, firebaseToken, resolutionAction.stripeToken);
              }

            });

          }else {

            (isDevel) ? console.log("ERROR The Source of the video is " + response.errorMessage) : 0;



          }
        });
      }
    }else {

      // Photo
      if(!optedInToADonation) {
        resolutionAction.attemptCompletePurchase(postId, STATUS_SUCCESS, firebaseToken, resolutionAction.stripeToken);
      }
    }


    $("#files").empty(); // .replaceWith($("#files").val('').clone(true));
    $('progress').attr('value', 0);

    $timeout(function() {
      $scope.$apply(function () {

        $scope.loading = false;
        $scope.image_chosen = false;
        $scope.isPageDisabled = true;
      });

    });




  }).on('fileuploadfail', function (e, data) {

    if(isDevel)console.log("Error " +JSON.stringify(e));

    if(isDevel)console.log("Data " +JSON.stringify(data));


    FoundationApi.publish('main-notifications-warning', {
      title: 'There was a problem',
      content: 'Sorry there was a problem with your post - try again later!'
    });


    $("#files").empty(); // .replaceWith($("#files").val('').clone(true));
    $('progress').attr('value', 0);


    $timeout(function() {

      $scope.$apply(function () {
        $scope.loading = false;
        $scope.image_chosen = false;
      });
    });


  }).prop('disabled', !$.support.fileInput)
    .parent().addClass($.support.fileInput ? undefined : 'disabled');




  $scope.myValidator = function (newValue) {
    // a simple required field:
    return !!newValue;
  };



}


/**
 * ng-inline-edit v0.7.0 (http://tamerayd.in/ng-inline-edit)
 * Copyright 2015 Tamer Aydin (http://tamerayd.in)
 * Licensed under MIT
 */
(function(window, angular, undefined) {
  'use strict';

  angular
    .module('angularInlineEdit.providers', [])
    .value('InlineEditConfig', {
      btnEdit: 'Edit',
      btnSave: '',
      btnCancel: '',
      editOnClick: false,
      onBlur: null
    })
    .constant('InlineEditConstants', {
      CANCEL: 'cancel',
      SAVE: 'save'
    });

})(window, window.angular);

(function(window, angular, undefined) {
  'use strict';

  angular
    .module('angularInlineEdit.controllers', [])
    .controller('InlineEditController', ['$scope', '$document', '$timeout',
      function($scope, $document, $timeout) {
        $scope.placeholder = '';
        $scope.validationError = false;
        $scope.validating = false;
        $scope.isOnBlurBehaviorValid = false;
        $scope.cancelOnBlur = false;
        $scope.editMode = false;
        $scope.inputValue = '';

        $scope.editText = function(inputValue) {
          $scope.editMode = true;
          $scope.inputValue = (typeof inputValue === 'string') ?
            inputValue : $scope.model;

          $timeout(function() {
            $scope.editInput[0].focus();
            if ($scope.isOnBlurBehaviorValid) {
              $document.bind('click', $scope.onDocumentClick);
            }
          }, 0);
        };

        $scope.applyText = function(cancel, byDOM) {
          var inputValue = $scope.inputValue; // initial input value
          $scope.validationError = false;

          function _onSuccess() {
            $scope.model = inputValue;
            $scope.callback({
              newValue: inputValue
            });

            $scope.editMode = false;
          }

          function _onFailure() {
            $scope.validationError = true;
            $timeout(function() {
              $scope.editText(inputValue);
            }, 0);
          }

          function _onEnd(apply) {
            $scope.validating = false;
            if (apply && byDOM) {
              $scope.$apply();
            }
          }

          if (cancel || $scope.model === inputValue) {
            $scope.editMode = false;
            if (byDOM) {
              $scope.$apply();
            }

          } else {
            $scope.validating = true;
            if (byDOM) {
              $scope.$apply();
            }

            var validationResult = $scope.validate({
              newValue: $scope.inputValue
            });

            if (validationResult && validationResult.then) { // promise
              validationResult
                .then(_onSuccess)
                .catch(_onFailure)
                .finally(_onEnd);

            } else if (validationResult ||
              typeof validationResult === 'undefined') {
              _onSuccess();
              _onEnd(true);

            } else {
              _onFailure();
              _onEnd(true);
            }
          }

          if ($scope.isOnBlurBehaviorValid) {
            $document.unbind('click', $scope.onDocumentClick);
          }
        };

        $scope.onInputKeyup = function(event) {
          if (!$scope.validating) {
            switch (event.keyCode) {
              case 13: // ENTER
                if ($scope.isInputTextarea) {
                  return;
                }
                $scope.applyText(false, false);
                break;
              case 27: // ESC
                $scope.applyText(true, false);
                break;
              default:
                break;
            }
          }
        };

        $scope.onDocumentClick = function(event) {
          if (!$scope.validating) {
            if (event.target !== $scope.editInput[0]) {
              $scope.applyText($scope.cancelOnBlur, true);
            }
          }
        };
      }
    ]);

})(window, window.angular);

(function(window, angular, undefined) {
  'use strict';

  angular
    .module('angularInlineEdit.directives', [
      'angularInlineEdit.providers',
      'angularInlineEdit.controllers'
    ])
    .directive('inlineEdit', ['$compile', 'InlineEditConfig', 'InlineEditConstants',
      function($compile, InlineEditConfig, InlineEditConstants) {
        return {
          restrict: 'A',
          controller: 'InlineEditController',
          scope: {
            model: '=inlineEdit',
            callback: '&inlineEditCallback',
            validate: '&inlineEditValidation'
          },
          link: function(scope, element, attrs) {
            scope.model = scope.$parent.$eval(attrs.inlineEdit);
            scope.isInputTextarea = attrs.hasOwnProperty('inlineEditTextarea');

            var onBlurBehavior = attrs.hasOwnProperty('inlineEditOnBlur') ?
              attrs.inlineEditOnBlur : InlineEditConfig.onBlur;
            if (onBlurBehavior === InlineEditConstants.CANCEL ||
              onBlurBehavior === InlineEditConstants.SAVE) {
              scope.isOnBlurBehaviorValid = true;
              scope.cancelOnBlur = onBlurBehavior === InlineEditConstants.CANCEL;
            }

            var container = angular.element(
              '<div class="ng-inline-edit" ' +
              'ng-class="{\'ng-inline-edit--validating\': validating, ' +
              '\'ng-inline-edit--error\': validationError}">');

            var input = angular.element(
              (scope.isInputTextarea ?
                '<textarea ' : '<input type="text" ') +
              'class="ng-inline-edit__input" ' +
              'ng-disabled="validating" ' +
              'ng-show="editMode" ' +
              'ng-keyup="onInputKeyup($event)" ' +
              'ng-model="inputValue" ' +
              'placeholder="{{placeholder}}" />');

            var innerContainer = angular.element(
              '<div class="ng-inline-edit__inner-container"></div>');

            // text
            innerContainer.append(angular.element(
              '<span class="ng-inline-edit__text" ' +
              'ng-class="{\'ng-inline-edit__text--placeholder\': !model}" ' +
              (attrs.hasOwnProperty('inlineEditOnClick') || InlineEditConfig.editOnClick ?
                'ng-click="editText()" ' : '') +
              'ng-if="!editMode">{{(model || placeholder)' +
              (attrs.hasOwnProperty('inlineEditFilter') ? ' | ' + attrs.inlineEditFilter : '') +
              '}}</span>'));

            // edit button
            var inlineEditBtnEdit = attrs.hasOwnProperty('inlineEditBtnEdit') ?
              attrs.inlineEditBtnEdit : InlineEditConfig.btnEdit;
            if (inlineEditBtnEdit) {
              innerContainer.append(angular.element(
                '<a class="ng-inline-edit__button ng-inline-edit__button--edit" ' +
                'ng-if="!editMode" ' +
                'ng-click="editText()">' +
                inlineEditBtnEdit +
                '</a>'));
            }

            // save button
            var inlineEditBtnSave = attrs.hasOwnProperty('inlineEditBtnSave') ?
              attrs.inlineEditBtnSave : InlineEditConfig.btnSave;
            if (inlineEditBtnSave) {
              innerContainer.append(angular.element(
                '<a class="ng-inline-edit__button ng-inline-edit__button--save" ' +
                'ng-if="editMode && !validating" ' +
                'ng-click="applyText(false, false)">' +
                inlineEditBtnSave +
                '</a>'));
            }

            // cancel button
            var inlineEditBtnCancel = attrs.hasOwnProperty('inlineEditBtnCancel') ?
              attrs.inlineEditBtnCancel : InlineEditConfig.btnCancel;
            if (inlineEditBtnCancel) {
              innerContainer.append(angular.element(
                '<a class="ng-inline-edit__button ng-inline-edit__button--cancel" ' +
                'ng-if="editMode && !validating" ' +
                'ng-click="applyText(true, false)">' +
                inlineEditBtnCancel +
                '</a>'));
            }

            container
              .append(input)
              .append(innerContainer);

            element
              .append(container);

            scope.editInput = input;

            attrs.$observe('inlineEdit', function(newValue) {
              scope.model = scope.$parent.$eval(newValue);
              $compile(element.contents())(scope);
            });

            attrs.$observe('inlineEditPlaceholder', function(placeholder) {
              scope.placeholder = placeholder;
            });

            scope.$watch('model', function(newValue) {
              if (!isNaN(parseFloat(newValue)) && isFinite(newValue) && newValue === 0) {
                scope.model = '0';
              }
            });
          }
        };
      }
    ]);

})(window, window.angular);

(function(window, angular, undefined) {
  'use strict';

  angular
    .module('angularInlineEdit', [
      'angularInlineEdit.providers',
      'angularInlineEdit.controllers',
      'angularInlineEdit.directives'
    ]);

})(window, window.angular);

app.controller('PrivacyCrtl', PrivacyCrtl);
PrivacyCrtl.$inject = ['$scope', '$stateParams', '$state', '$controller'];

function PrivacyCrtl($scope, $stateParams, $state, $controller) {

  angular.extend(this, $controller('DefaultController', {$scope: $scope, $stateParams: $stateParams, $state: $state}));

  $scope.name = "john";
}


app.controller('TermsCtlr', TermsCtlr);
TermsCtlr.$inject = ['$scope', '$stateParams', '$state', '$controller'];

function TermsCtlr($scope, $stateParams, $state, $controller) {
  angular.extend(this, $controller('DefaultController', {$scope: $scope, $stateParams: $stateParams, $state: $state}));


  $scope.name = "john";
}




app.controller('YourResolutionCtlr', YourResolutionCtlr);

YourResolutionCtlr.$inject = ['$scope', '$log', 'oauth2Provider', 'HTTP_ERRORS', 'fbauthenticationProvider'];


function YourResolutionCtlr ($scope, $log, oauth2Provider, HTTP_ERRORS, fbauthenticationProvider) {

  const SIGNED_OUT = "Signed Out";
  const SIGNED_IN = "Signed In";
  $scope.loading = false;
  $scope.resolutions = [];

  //  var isDevel = true;



  if(isDevel)console.log('oauth2Provider.isUserSignedIn ' + oauth2Provider.isUserSignedIn());

  $scope.message = (oauth2Provider.isUserSignedIn()) ? SIGNED_IN : SIGNED_OUT;

  if(isDevel)console.log('MESSAGE ' + $scope.message);


  $scope.signIn = function () {

    if(isDevel)console.log('Sign In');
    $scope.loading = true;

    oauth2Provider.signIn(function (response) {

      if(isDevel)console.log('isLoggedIn ' + response.loggedIn);

      oauth2Provider.signedIn(response.loggedIn);

      var isLoggedIn = oauth2Provider.isUserSignedIn();
      if(isDevel)console.log('isLoggedIn after' + isLoggedIn);

      $scope.$apply(function (response) {

        if(isDevel)console.log('Inside Apply of signIn ' + isLoggedIn);

        if(isLoggedIn){
          $scope.message = SIGNED_IN;
        }
        $scope.loading = false;

      });

    });

  };

  $scope.signOut = function () {
    if(isDevel)console.log('Sign Out');
    $scope.loading = true;

    oauth2Provider.signOut(function (response) {

      oauth2Provider.signedIn(response.loggedIn);

      var isLoggedIn = oauth2Provider.isUserSignedIn();

      $scope.$apply(function (response) {

        if(isDevel)console.log('Inside Apply of signout ' + isLoggedIn);
        if(isDevel)console.log("Signing Out ... ");

        if(!isLoggedIn ) {
          $scope.message = SIGNED_OUT;
        }
        $scope.loading = false;


      })
    });
  }


  /*
   *
   */




  $scope.authorize = function () {

    var userIdToken = oauth2Provider.getFirebaseToken();
    $scope.loading = true;

    gapi.client.conference.queryResolution({
      firebaseToken : userIdToken
    }).
    execute(function (resp) {

      if(resp.error) {
        if(isDevel) console.log("Error" + JSON.stringify(resp, null, '  '))

      }else {
        if(isDevel) console.log("Success" + JSON.stringify(resp, null, '  '))
      }

      $scope.$apply(function () {

        $scope.loading = false;

      });

    });
    // [END authstatelistener]
  }


};


var ScrollButtons = {
  scrollTo: function (link, elem) {
      $('html, body').animate({
        scrollTop: $('#' + elem).offset().top
      }, 500);

  },
  initScroll: function (link, elem) {
    this.scrollTo(link, elem);
  }

};
