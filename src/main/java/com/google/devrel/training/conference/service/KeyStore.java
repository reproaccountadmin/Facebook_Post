package com.google.devrel.training.conference.service;

import com.googlecode.objectify.Key;

public class KeyStore<K> {

	public KeyStore() {
		// TODO Auto-generated constructor stub
	}
	
    public Key key(String id, Class c) {
    	Key<K> key = Key.create(c, id);
    	return (Key<K>) key;
    }
	

}
