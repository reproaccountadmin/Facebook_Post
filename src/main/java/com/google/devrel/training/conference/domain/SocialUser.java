package com.google.devrel.training.conference.domain;

import java.util.logging.Logger;

import com.google.devrel.training.auth.AppengineFirebaseToken;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

@Entity
@Cache
public class SocialUser{

    private static final Logger LOG = Logger.getLogger(SocialUser.class.getName());


	private String _displayName;
	private String _email;
    private Key<ChargePaymentDetails> chargeKey;
	
    @Id 
	String id;

    
	private SocialUser(){};
	
	
	public SocialUser(String userId, String displayName, String mainEmail) {
		_displayName = displayName;
		_email = mainEmail;
		id = userId;
		LOG.info("Creating Social User");
	}
	
    public String getEmail() {
		return _email;
	}

	public String getDisplayName() {
		return _displayName;
	}
	
	public void addChargePaymentKey(Key<ChargePaymentDetails> chargePaymentsKey) {
		chargeKey = chargePaymentsKey;
	}
	
	public Key<ChargePaymentDetails> getChargePaymentKey() {
		return chargeKey;
	}
	
	public void updateSocialUser( String displayName, String mainEmail) {
		if(displayName != null)_displayName = displayName;
		if(mainEmail != null)_email = mainEmail;
	}

}
