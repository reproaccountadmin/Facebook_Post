package com.google.devrel.training.conference.spi;


import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiMethod.HttpMethod;
import com.google.api.server.spi.response.ConflictException;
import com.google.api.server.spi.response.ForbiddenException;
import com.google.api.server.spi.response.NotFoundException;
import com.google.api.server.spi.response.UnauthorizedException;
import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.users.User;
import com.google.devrel.training.auth.AppengineFirebaseToken;
import com.google.devrel.training.auth.FirebaseAuthUtility;
import com.google.devrel.training.conference.Constants;
import com.google.devrel.training.conference.domain.Announcement;
import com.google.devrel.training.conference.domain.AppEngineUser;
import com.google.devrel.training.conference.domain.Conference;
import com.google.devrel.training.conference.domain.ChargePaymentDetails;
import com.google.devrel.training.conference.domain.Profile;
import com.google.devrel.training.conference.domain.Resolution;
import com.google.devrel.training.conference.domain.SocialUser;
import com.google.devrel.training.conference.form.ConferenceForm;
import com.google.devrel.training.conference.form.ConferenceQueryForm;
import com.google.devrel.training.conference.form.ChargePaymentForm;
import com.google.devrel.training.conference.form.ProfileForm;
import com.google.devrel.training.conference.form.ProfileForm.TeeShirtSize;
import com.google.devrel.training.conference.form.ResolutionForm;
import com.google.devrel.training.conference.service.DataSource;
import com.google.devrel.training.conference.service.KeyStore;
import com.google.devrel.training.conference.service.OfyService;
import com.google.devrel.training.stripe.StripeCharge;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseToken;
import com.google.firebase.tasks.OnFailureListener;
import com.google.firebase.tasks.OnSuccessListener;
import com.google.firebase.tasks.Task;
import com.google.firebase.tasks.Tasks;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.Work;

import static com.google.devrel.training.conference.service.OfyService.factory;
import static com.google.devrel.training.conference.service.OfyService.ofy;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

import javax.inject.Named;

import com.googlecode.objectify.cmd.Query;
import com.stripe.Stripe;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.exception.RateLimitException;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

/**
 * Defines conference APIs.
 */
@Api(name = "conference", version = "v1", 
	scopes = {Constants.EMAIL_SCOPE },
    clientIds = {
		Constants.WEB_CLIENT_ID,
		Constants.API_EXPLORER_CLIENT_ID },
    audiences = {Constants.WEB_CLIENT_ID},
    description = "API for the New Year Resolution Challenge Backend application.")

public class ConferenceApi {

	public ConferenceApi() {
		
		try{
		FirebaseOptions options = new FirebaseOptions.Builder()
				  .setServiceAccount(new FileInputStream("WEB-INF/serviceAccountKey.json"))
				  .setDatabaseUrl("https://social-challenge-148216.firebaseio.com/")
				  .build();
				  
		FirebaseApp.initializeApp(options);
		}catch(FileNotFoundException e) {
			
		}
	}
	
	

	/*
	 * Get the display name from the user's email. For example, if the email is
	 * lemoncake@example.com, then the display name becomes "lemoncake."
	 */
	private static String extractDefaultDisplayNameFromEmail(String email) {
		return email == null ? null : email.substring(0, email.indexOf("@"));
	}
	
    private static final Logger LOG = Logger.getLogger(ConferenceApi.class.getName());

	
    private static SocialUser getSocialUserFromUser(User user, String userId) {
        // First fetch it from the datastore.
    	SocialUser socialUser = ofy().load().key(
                Key.create(SocialUser.class, userId)).now();
        if (socialUser == null) {
            // Create a new Profile if not exist.
            String email = user.getEmail();
            socialUser = new SocialUser(userId, extractDefaultDisplayNameFromEmail(email), email);
        }
        return socialUser;
    }
    
    
    /*
    
    private static Profile getProfileFromUser(User user, String userId) {
        // First fetch it from the datastore.
        Profile profile = ofy().load().key(
                Key.create(Profile.class, userId)).now();
        if (profile == null) {
            // Create a new Profile if not exist.
            String email = user.getEmail();
            profile = new Profile(userId,
                    extractDefaultDisplayNameFromEmail(email), email, TeeShirtSize.NOT_SPECIFIED);
        }
        return profile;
    }

    
    
    private static Profile getProfileFromUser(User user) {
    	
    	return ofy().load().key(Key.create(Profile.class, user.getUserId())).now();
    }
    
	
    // Declare this method as a method available externally through Endpoints
	@ApiMethod(name = "saveProfile", path = "profile", httpMethod = HttpMethod.POST)
	
	// The request that invokes this method should provide data that
	// conforms to the fields defined in ProfileForm

	// TODO 1 Pass the ProfileForm parameter
	// TODO 2 Pass the User parameter
	public Profile saveProfile(ProfileForm profileForm)
			throws UnauthorizedException {

		LOG.info("Profile Form " + profileForm.getTeeShirtSize());
		

 
		String token =  profileForm.getDisplayName();
		LOG.info("******* TOKEN ****** " + token);


		User user = null;
			
			Task<FirebaseToken> authTask = FirebaseAuth.getInstance().verifyIdToken(token).addOnSuccessListener(new OnSuccessListener<FirebaseToken>() {
				
						@Override
					        public void onSuccess(FirebaseToken decodedToken) {
					        	 String uid = decodedToken.getUid();
						    		LOG.info("User ID " + uid);
					        }
					        
					    }).addOnFailureListener(new OnFailureListener() {
					        @Override
					        public void onFailure(Exception excptn) {
								LOG.info("Failure " + excptn.getMessage());

					        }
					    });

	
			
		        try {
					Tasks.await(authTask);
				} catch (ExecutionException | InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		
		        if(!authTask.isSuccessful()) {
		        	throw new UnauthorizedException("Authorization required");
		        }
		        
		        
		    FirebaseToken decodedToken = authTask.getResult();
		    
    		LOG.info("User ID from Decoded Token " + decodedToken.getUid());
	
    		
		if(profileForm != null) {
			return new Profile("3434324323", profileForm.getDisplayName(), "vanessa.nutridirect@gmail.com", profileForm.getTeeShirtSize());
		}
		

		// TODO 2
		// If the user is not logged in, throw an UnauthorizedException
		if (user == null) {
            throw new UnauthorizedException("Authorization required");
        }

		// TODO 1
	    // Set the teeShirtSize to the value sent by the ProfileForm, if sent
        // otherwise leave it as the default value

		// TODO 1
        // Set the displayName to the value sent by the ProfileForm, if sent
        // otherwise set it to null
		 String displayName = profileForm.getDisplayName();
		 TeeShirtSize teeShirtSize = profileForm.getTeeShirtSize();
		// TODO 2
		// Get the userId and mainEmail
		String mainEmail = user.getEmail();
		String userId = user.getUserId();

        // TODO 2
        // If the displayName is null, set it to the default value based on the user's email
        // by calling extractDefaultDisplayNameFromEmail(...)

		Profile profile = getProfileFromUser(user);
		
		
		// Create a new Profile entity from the
		// userId, displayName, mainEmail and teeShirtSize
		if(profile == null){
			
			 if (displayName == null) {
				 displayName = extractDefaultDisplayNameFromEmail(user.getEmail());
			 }
			 

			 if (teeShirtSize == null) {
				 teeShirtSize = TeeShirtSize.NOT_SPECIFIED;
			 }

			profile = new Profile(userId, displayName, mainEmail, teeShirtSize);
		
		
		}else {
			
			profile.update(displayName, teeShirtSize);
			
		}
		// TODO 3 (In lesson 3)
		// Save the entity in the datastore
		

		 OfyService.ofy().save().entity(profile).now();
		
		// Return the profile
		return profile;
	}

	

	@ApiMethod(name = "getProfile", path = "profile", httpMethod = HttpMethod.GET)
	public Profile getProfile(final User user) throws UnauthorizedException {
		if (user == null) {
			throw new UnauthorizedException("Authorization required");
		}

		// TODO
		// load the Profile Entity
		String userId = user.getUserId();

		return  OfyService.ofy().load().key(Key.create(Profile.class, userId)).now();

	}
	

    @ApiMethod(name = "createConference", path = "conference", httpMethod = HttpMethod.POST)
    public Conference createConference(final User user, final ConferenceForm conferenceForm)
        throws UnauthorizedException {
       
    	if (user == null) {
            throw new UnauthorizedException("Authorization required");
        }

    	final String userId = getUserId(user);
        final Key<Profile> profileKey = new KeyStore<Profile>().key(userId, Profile.class);
        final Key<Conference> conferenceKey = ObjectifyService.factory().allocateId(profileKey, Conference.class);
        final long conferenceId = conferenceKey.getId();
        final Queue queue = QueueFactory.getDefaultQueue();
      
        Conference conference = ofy().transact(new Work<Conference>() {
        
        
        @Override 
        public Conference run(){
        	
			
			Profile profile = getProfileFromUser(user, userId);
			
	        Conference conference = new Conference(conferenceId, userId, conferenceForm);
	        
	        LOG.info("Conference object = " + conference + "and Profile " + profile);
	        
	        OfyService.ofy().save().entities(conference, profile).now();
	       
	        queue.add(ofy().getTransaction(), TaskOptions.Builder.withUrl("/tasks/send_email_announcement")
	        		.param("email", profile.getMainEmail())
	        		.param("conferenceInfo", conference.toString()));
	        
	        return conference;
        }
        
        });
        
        
        
         return conference;
         }

    @ApiMethod(
            name = "queryConferences",
            path = "queryConferences",
            httpMethod = HttpMethod.POST
    )
    public List<Conference> queryConferences(ConferenceQueryForm conferenceQueryForm) {
        Iterable<Conference> conferenceIterable = conferenceQueryForm.getQuery();
        List<Conference> result = new ArrayList<>(0);
        List<Key<Profile>> organizersKeyList = new ArrayList<>(0);
        for (Conference conference : conferenceIterable) {
            organizersKeyList.add(Key.create(Profile.class, conference.getOrganizerUserId()));
            result.add(conference);
        }
        // To avoid separate datastore gets for each Conference, pre-fetch the Profiles.
        ofy().load().keys(organizersKeyList);
        return result;
        
    }
    
    
    @ApiMethod(
    		name = "getConferencesCreated", 
    		path = "getConferencesCreated", 
    		httpMethod = HttpMethod.POST
    		)
    
    public List<Conference> getConferencesCreated(final User user) throws UnauthorizedException{
    
    	if(user == null) {
    		throw new UnauthorizedException("Authorization required");
    	}
    	
    	String userId = user.getUserId();
    	return ofy().load().type(Conference.class).ancestor(new KeyStore<Profile>().key(userId, Profile.class)).order("name").list();
    }
    
    
    @ApiMethod(
    		name = "filterHeaven", 
    		path = "filterHeaven"
    		)
    
    public List<Conference> filterHeaven() {
    	
    	Query<Conference> query = ofy().load().type(Conference.class).filter("maxAttendees <", 25).filter("city =", "London");
    	return query.list();
    }
    
    
    @ApiMethod(
    		name = "filterPlayground", 
    		path = "filterPlayground"
    		)
    
    public List<Conference> filterPlayground(){
   
    	
    	  Query<Conference> query = ofy().load().type(Conference.class);
    	  
    	  query = query.filter("city = ", "Tokyo");
    	  query = query.filter("seatsAvailable >", 0);
    	  query = query.filter("seatsAvailable <", 10).order("seatsAvailable").order("name").order("month");

    	  return query.list();
    }

    */
    
    

    public static class WrappedBoolean {

        private final Boolean result;
        private final String reason;

        public WrappedBoolean(Boolean result) {
            this.result = result;
            this.reason = "";
        }

        public WrappedBoolean(Boolean result, String reason) {
            this.result = result;
            this.reason = reason;
        }

        public Boolean getResult() {
            return result;
        }

        public String getReason() {
            return reason;
        }
    }
    
    /*
    @ApiMethod(
            name = "registerForConference",
            path = "conference/{websafeConferenceKey}/registration",
            httpMethod = HttpMethod.POST
    )

    public WrappedBoolean registerForConference(final User user,  @Named("websafeConferenceKey") final String websafeConferenceKey)
            throws UnauthorizedException, NotFoundException,
            ForbiddenException, ConflictException {
        // If not signed in, throw a 401 error.
        if (user == null) {
            throw new UnauthorizedException("Authorization required");
        }

        // Get the userId
        final String userId = user.getUserId();

        // TODO
        // Start transaction
        WrappedBoolean result = ofy().transact(new Work<WrappedBoolean>() {
        	
        	@Override
        	public WrappedBoolean run(){
                try {

                // TODO
                // Get the conference key -- you can get it from websafeConferenceKey
                // Will throw ForbiddenException if the key cannot be created
                Key<Conference> conferenceKey = Key.create(websafeConferenceKey);

                // TODO
                // Get the Conference entity from the datastore
                Conference conference = ofy().load().key(conferenceKey).now();

                // 404 when there is no Conference with the given conferenceId.
                if (conference == null) {
                    return new WrappedBoolean (false,
                            "No Conference found with key: "
                                    + websafeConferenceKey);
                }

                // TODO
                // Get the user's Profile entity
                Profile profile = getProfileFromUser(user, userId);

                // Has the user already registered to attend this conference?
                if (profile.getConferenceKeysToAttend().contains(
                        websafeConferenceKey)) {
                    return new WrappedBoolean (false, "Already registered");
                } else if (conference.getSeatsAvailable() <= 0) {
                    return new WrappedBoolean (false, "No seats available");
                } else {
                    // All looks good, go ahead and book the seat
                    
                    // TODO
                    // Add the websafeConferenceKey to the profile's
                    // conferencesToAttend property
                    profile.addToConferenceKeysToAttend(websafeConferenceKey);
                    
                    // TODO 
                    // Decrease the conference's seatsAvailable
                    // You can use the bookSeats() method on Conference
                    conference.bookSeats(1);
                    
                    
                    // TODO
                    // Save the Conference and Profile entities
                    
                    ofy().save().entities(conference, profile).now();
                    
                    
                    // We are booked!
                    return new WrappedBoolean(true, "Registration successful");
                }

                }
                catch (Exception e) {
                    return new WrappedBoolean(false, "Unknown exception");
                }
    }
    });

        // if result is false
        if (!result.getResult()) {
            if (result.getReason().contains("No Conference found with key")) {
                throw new NotFoundException (result.getReason());
            }
            else if (result.getReason() == "Already registered") {
                throw new ConflictException("You have already registered");
            }
            else if (result.getReason() == "No seats available") {
                throw new ConflictException("There are no seats available");
            }
            else {
                throw new ForbiddenException("Unknown exception");
            }
        }
        return result;
    }
    

    @ApiMethod(
            name = "getConference",
            path = "conference/{websafeConferenceKey}",
            httpMethod = HttpMethod.GET
    )
    public Conference getConference(
            @Named("websafeConferenceKey") final String websafeConferenceKey)
            throws NotFoundException {
       
    	
    	Key<Conference> conferenceKey = Key.create(websafeConferenceKey);
        Conference conference = ofy().load().key(conferenceKey).now();
        if (conference == null) {
            throw new NotFoundException("No Conference found with key: " + websafeConferenceKey);
        }
        return conference;
    }

    @ApiMethod(
            name = "getConferencesToAttend",
            path = "getConferencesToAttend",
            httpMethod = HttpMethod.GET
    )
    public Collection<Conference> getConferencesToAttend(final User user)
            throws UnauthorizedException, NotFoundException {
        // If not signed in, throw a 401 error.
        if (user == null) {
            throw new UnauthorizedException("Authorization required");
        }
        // TODO
        // Get the Profile entity for the user
        Profile profile = (Profile)new DataSource<Profile>().load(user.getUserId(), Profile.class);
       
        if (profile == null) {
            throw new NotFoundException("Profile doesn't exist.");
        }

        // TODO
        // Get the value of the profile's conferenceKeysToAttend property
        List<String> keyStringsToAttend = profile.getConferenceKeysToAttend(); // change this

        // TODO
        // Iterate over keyStringsToAttend,
        // and return a Collection of the
        // Conference entities that the user has registered to attend
        
        List<Key<Conference>> keys = new ArrayList<>();
       
        for(String keyString: keyStringsToAttend) {
        	
        	keys.add(Key.<Conference>create(keyString));
        	
        }
        
        return  ofy().load().keys(keys).values();
        		
    }
    

    @ApiMethod(
            name = "unregisterFromConference",
            path = "conference/{websafeConferenceKey}/registration",
            httpMethod = HttpMethod.DELETE
    )
    public WrappedBoolean unregisterFromConference(final User user,
                                            @Named("websafeConferenceKey")
                                            final String websafeConferenceKey)
            throws UnauthorizedException, NotFoundException, ForbiddenException, ConflictException {
        // If not signed in, throw a 401 error.
        if (user == null) {
            throw new UnauthorizedException("Authorization required");
        }

        WrappedBoolean result = ofy().transact(new Work<WrappedBoolean>() {
            @Override
            public WrappedBoolean run() {
                Key<Conference> conferenceKey = Key.create(websafeConferenceKey);
                Conference conference = ofy().load().key(conferenceKey).now();
                // 404 when there is no Conference with the given conferenceId.
                if (conference == null) {
                    return new  WrappedBoolean(false,
                            "No Conference found with key: " + websafeConferenceKey);
                }

                // Un-registering from the Conference.
                Profile profile = getProfileFromUser(user, user.getUserId());
                if (profile.getConferenceKeysToAttend().contains(websafeConferenceKey)) {
                    profile.unregisterFromConference(websafeConferenceKey);
                    conference.giveBackSeats(1);
                    ofy().save().entities(profile, conference).now();
                    return new WrappedBoolean(true);
                } else {
                    return new WrappedBoolean(false, "You are not registered for this conference");
                }
            }
        });
        // if result is false
        if (!result.getResult()) {
            if (result.getReason().contains("No Conference found with key")) {
                throw new NotFoundException (result.getReason());
            }
            else {
                throw new ForbiddenException(result.getReason());
            }
        }
        // NotFoundException is actually thrown here.
        return new WrappedBoolean(result.getResult());
    }
    
    @ApiMethod(
            name = "getAnnouncements",
            path = "announcement",
            httpMethod = HttpMethod.GET
    )
    
    public Announcement getAnnouncement() {
    	
        MemcacheService memcacheService =  MemcacheServiceFactory.getMemcacheService();
        final String MEMCACHE_KEY = Constants.MEMCACHE_ANNOUNCEMENTS_KEY;
    	
        Object message = memcacheService.get(MEMCACHE_KEY);
        
    	if(message != null) {
    		return new Announcement(message.toString());
    	}
        
        
    	return null;
    }
    
	
	*/


	
	 @ApiMethod(name = "attemptChargeOnCard", path = "attemptChargeOnCard", httpMethod = HttpMethod.POST)
	 public WrappedBoolean attemptChargeOnCard(@Named("postId") String postId, @Named("status") String status, @Named("firebaseToken") String firebaseToken, @Named("stripeToken") String stripeToken) 
			 throws UnauthorizedException, 
			 IllegalArgumentException, 
			 AuthenticationException, 
			 InvalidRequestException, 
			 APIConnectionException, 
			 APIException,
			 RateLimitException,
			 StripeException {
		  
		boolean result = false;
		String token = firebaseToken;	
		FirebaseToken decodedToken = FirebaseAuthUtility.FirebaseToken(token);
		 
		LOG.info("User ID from Decoded Token " + decodedToken.getUid());
		
		final AppengineFirebaseToken fbToken = new AppengineFirebaseToken(decodedToken);			 
		User user = new User(fbToken.getEmail(), Constants.AUTH_DOMAIN, fbToken.getUid());

	
		final String userId = user.getUserId();

		 
		 LOG.info("The USER ID IS ****** " + userId);
		 
		
      // Giving the Entity an id then saving...

		// List<ChargePaymentDetails> set = OfyService.ofy().load().type(ChargePaymentDetails.class).ancestor(new KeyStore<SocialUser>().key(userId, SocialUser.class)).order("lastName").list();
		 
		// LOG.info("List length is " + set.size());
		 
		 
		 ChargePaymentDetails chargePaymentDetails = OfyService.ofy().load().type(ChargePaymentDetails.class).filter("stripeToken", stripeToken).first().now();


		 if(chargePaymentDetails != null) {
			 
			
			 try {
			 
				 
				 Charge charge = StripeCharge.ChargeCard(chargePaymentDetails.getStripePurchase());
				 
				 String stipeAuthToken = charge.getId();
				 
				 chargePaymentDetails.updateChargeDetails(status, postId, stipeAuthToken);
				 OfyService.ofy().save().entity(chargePaymentDetails).now();
			
			 
			 }catch(Exception e) {
				 return new WrappedBoolean(false, "Your payment failed. Please contact us quoting this reference " + postId);
			 }
			
		 }else {
			 
             return new WrappedBoolean(false, "No Charge Payment Found");

		 }

		
	       return new WrappedBoolean(true, "Charge Payment was successful");
	 
	 }
	 
	

	 @ApiMethod(name = "createChargePaymentDetails", path = "createChargePaymentDetails", httpMethod = HttpMethod.POST)
	 public WrappedBoolean createChargePaymentDetails(final ChargePaymentForm chargePaymentForm) 
	    		throws UnauthorizedException, IllegalArgumentException {
	
		 LOG.info("Coming into the Method");
		 LOG.info("The Charge Payment Form " + chargePaymentForm.toString());
	
		 final String stripeToken = chargePaymentForm.getStripeToken();
			LOG.info("Stripe Token " + stripeToken);

		 if(stripeToken == null){
            return new WrappedBoolean(false, "Stripe Token not provided");
		 }
		 
		 
		String token = chargePaymentForm.getFirebaseToken();	
		FirebaseToken decodedToken = FirebaseAuthUtility.FirebaseToken(token);
		 
		LOG.info("User ID from Decoded Token " + decodedToken.getUid());
		
		final AppengineFirebaseToken fbToken = new AppengineFirebaseToken(decodedToken);			 

		 /*
		  * 
		  * 
		  */
		 
        
		 WrappedBoolean result = ofy().transact(new Work<WrappedBoolean>() {
	        	
	        	@Override
	        	public WrappedBoolean run(){
	                try {

	            //        return new WrappedBoolean (false, "No Conference found with key: "  + websafeConferenceKey);
	            	
	                	final User user = new User(fbToken.getEmail(), Constants.AUTH_DOMAIN, fbToken.getUid());
	            		
	            		final String userId = user.getUserId();
	            		 
	            		 LOG.info("The USER ID IS ****** " + userId);
	            		 
	            	    final Key<SocialUser> socialUserKey = Key.create(SocialUser.class, userId);
	            	    final Key<ChargePaymentDetails> chargePaymentKey = ObjectifyService.factory().allocateId(socialUserKey, ChargePaymentDetails.class);
	            	    final long chargePaymentId = chargePaymentKey.getId();
	            		 
	            		 
	            		SocialUser socialUser = getSocialUserFromUser(user, userId);        		 
	           		
	            		ChargePaymentDetails chargePaymentDetails = new ChargePaymentDetails(chargePaymentId, userId, chargePaymentForm, stripeToken);	      


	                    
	            		OfyService.ofy().save().entities(chargePaymentDetails, socialUser).now();
	                    
	                    
	                    // We are booked!
	                    return new WrappedBoolean(true, "Creating Charge Payment Details was successful");
	             

	                }
	                catch (Exception e) {
	                	LOG.info("E " + e.getMessage());
	                	e.printStackTrace();
	                    return new WrappedBoolean(false, "Unknown exception");
	                }
	    }
	    });
	

		   return result;
  }

	  
    
	
}
