package com.google.devrel.training.conference.domain;

import java.util.Date;
import java.util.logging.Logger;

import com.google.api.server.spi.config.AnnotationBoolean;
import com.google.api.server.spi.config.ApiResourceProperty;
import com.google.devrel.training.conference.form.ChargePaymentForm;
import com.google.devrel.training.stripe.StripePurchase;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Parent;

@Cache
@Entity
public class ChargePaymentDetails {

	private static final Logger LOG = Logger.getLogger(ChargePaymentDetails.class.getName());

	@Id 
	private Long id;

    private boolean optOut;

    private boolean optInGiftAid;
	
    private boolean myOwnMoney;
    
    private boolean noGambling;

    private Date creationDate;
    
	private String title;
	
	private String firstName;

	@Index
	private String lastName;

	private String address;

	private String postcode;


	private Double amountDonated;
	
	private String currency;

	private String facebookArticleID;

	private String status;

	@Index
	private String stripeToken;

    @Parent
    @ApiResourceProperty(ignored = AnnotationBoolean.TRUE)
    private Key<SocialUser> socialUserKey;

    @Index
	private String stripeAuthToken;
	
	private ChargePaymentDetails(){};
	
	public ChargePaymentDetails(final Long id, final String socialUserId  , final ChargePaymentForm chargePaymentForm, String stripeToken) {
        this.id = id;
        this.socialUserKey = Key.create(SocialUser.class, socialUserId);
        this.creationDate = new Date();
        this.stripeToken = stripeToken;
        updateWithChargePaymentForm(chargePaymentForm);
       
        LOG.info("Charge Payment Details Created...");
	}
	
	

	 public void updateWithChargePaymentForm(ChargePaymentForm chargePaymentForm) {
	        this.optOut = chargePaymentForm.isOptOut();
	        this.optInGiftAid = chargePaymentForm.isOptInGiftAid();
	        this.myOwnMoney = chargePaymentForm.isMyOwnMoney();
	        this.noGambling = chargePaymentForm.isNoGambling();
	        this.title = chargePaymentForm.getTitle();
	        this.firstName = chargePaymentForm.getFirstName();
	        this.lastName = chargePaymentForm.getFirstName();
	        this.address = chargePaymentForm.getAddress();
	        this.postcode = chargePaymentForm.getPostcode();
	        this.amountDonated = chargePaymentForm.getAmountDonated();
	        this.currency = chargePaymentForm.getCurrency();
	        this.facebookArticleID = chargePaymentForm.getFacebookArticleID(); 
	        this.status = chargePaymentForm.getStatus();
	    }


	 
	 public void updateChargeDetails(final String status, final String facebookId, final String stipeAuthToken) {
		 this.status = status;
		 this.facebookArticleID = facebookId;
		 this.stripeAuthToken = stipeAuthToken;
	 }
	 

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}



	/**
	 * @return the optOut
	 */
	public boolean isOptOut() {
		return optOut;
	}



	/**
	 * @return the optInGiftAid
	 */
	public boolean isOptInGiftAid() {
		return optInGiftAid;
	}



	/**
	 * @return the myOwnMoney
	 */
	public boolean isMyOwnMoney() {
		return myOwnMoney;
	}



	/**
	 * @return the noGambling
	 */
	public boolean isNoGambling() {
		return noGambling;
	}



	/**
	 * @return the creationDate
	 */
	public Date getCreationDate() {
		return creationDate;
	}



	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}



	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}



	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}



	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}



	/**
	 * @return the postcode
	 */
	public String getPostcode() {
		return postcode;
	}



	/**
	 * @return the amountDonated
	 */
	public Double getAmountDonated() {
		return amountDonated;
	}
	
	public int getAmountDonatedRounded () {
		
		int amountDonated = -1;
		
		try{
			amountDonated = ( new Double(this.getAmountDonated() * 100).intValue() );
		}catch(NullPointerException e){
			
		}
		
		return  (amountDonated != -1) ? amountDonated : 0;
	}



	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}



	/**
	 * @return the permalink
	 */
	public String getFacebookArticleID() {
		return facebookArticleID;
	}



	

	public String getStatus() {
		return status;
	}



	public String getStripeToken() {
		return stripeToken;
	}


	public StripePurchase getStripePurchase() {
		
		return new StripePurchase(
				this.getStripeToken(), 
				this.getAmountDonatedRounded(), 
				this.getCurrency());	
	}

	public String getStripeAuthToken() {
		return stripeAuthToken;
	}
	
	

}
