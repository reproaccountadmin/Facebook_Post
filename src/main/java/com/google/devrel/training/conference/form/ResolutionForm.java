package com.google.devrel.training.conference.form;

import com.google.common.collect.ImmutableList;
import com.googlecode.objectify.annotation.Ignore;

import java.util.Date;
import java.util.List;

/**
 * A simple Java object (POJO) representing a Conference form sent from the client.
 */
public class ResolutionForm {
    /**
     * The name of the conference.
     */
    private String _name;

    /**
     * The resolution
     */
    private String _resolution;
	
    private String _challenge;

    private List<String> _friends;

    /**
     * The start date of the conference.
     */
    private Date _startDate;

	
	private Double _amountDonated;
	
	private String _videoUrl;
	
	@Ignore
	private String _firebaseToken;

    
    public ResolutionForm() {}

    /**
     * Public constructor is solely for Unit Test.
     * @param name
     * @param description
     * @param topics
     * @param city
     * @param startDate
     * @param endDate
     * @param maxAttendees
     */
    public ResolutionForm(String name, String resolution, List<String> friends,
                          Date startDate, String challenge, Double donation, String videoUrl, String firebaseToken) {
        this._name = name;
        this._resolution = resolution;
        this._startDate = startDate == null ? null : new Date(startDate.getTime());
        this._challenge = challenge;
        this._amountDonated = donation;
        this._videoUrl = videoUrl;
        this._firebaseToken = firebaseToken;
        this._friends = friends == null ? null : ImmutableList.copyOf(friends);

    }

    public String getName() {
        return _name;
    }

    public String getResolution() {
        return _resolution;
    }
    
	public String getChallenge() {
		return _challenge;
	}
	
    public Date getStartDate() {
        return _startDate;
    }



	public Double getAmountDonated() {
		return _amountDonated;
	}

	public String getVideoUrl() {
		return _videoUrl;
	}

	public void setChallenge(String challenge) {
		this._challenge = challenge;
	}

	public void setAmountDonated(Double amountDonated) {
		this._amountDonated = amountDonated;
	}

	public void setVideoUrl(String videoUrl) {
		this._videoUrl = videoUrl;
	}
	
	
	public String getFirebaseToken() {
		return _firebaseToken;
	}
	
	public void setFirebaseToken(String firebaseToken) {
		this._firebaseToken = firebaseToken;
	}
	
	public List<String> getFriends() {
		return _friends;
	}

}
