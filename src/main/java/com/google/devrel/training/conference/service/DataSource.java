package com.google.devrel.training.conference.service;

import static com.google.devrel.training.conference.service.OfyService.ofy;


import com.googlecode.objectify.Key;

public class DataSource<K> {
	
	
	public DataSource(){};
	
    public Object saveEntity(Object e) {
		ofy().save().entity(e).now();
		return e;
    }

    public Object load(String userId, Class c) {
    	return ofy().load().key(new KeyStore().key(userId, c)).now();
    }
    
	
	

}
