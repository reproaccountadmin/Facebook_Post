package com.google.devrel.training.conference.domain;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import com.google.api.server.spi.config.AnnotationBoolean;
import com.google.api.server.spi.config.ApiResourceProperty;
import com.google.common.collect.ImmutableList;
import com.google.devrel.training.conference.form.ResolutionForm;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Parent;


@Entity
@Cache
public class Resolution {

    private static final Logger LOG = Logger.getLogger(Resolution.class.getName());

	@Id 
	private Long id;
	

	private String _name;
	private String _resolution;
	private String _challenge;
	
	private Date _dateCreated;

	private Double _amountDonated;
	private String _videoUrl;
	private String _organizerUserId;
    private List<String> _friends;
    private static final List<String> DEFAULT_FRIENDS = ImmutableList.of("");

	
	@Parent
	@ApiResourceProperty(ignored=AnnotationBoolean.TRUE)
	private Key<SocialUser> socialUserKey;
	
	public Resolution(final long id, final String organizerUserId, final ResolutionForm resolutionForm) {
        this.id = id;
        this.socialUserKey = Key.create(SocialUser.class, organizerUserId);
        this._organizerUserId = organizerUserId;
       
        updateWithResolutionForm(resolutionForm);
        LOG.info("Resolution Created...");
	}
	
	

	 public void updateWithResolutionForm(ResolutionForm resolutionForm) {
	        this._name = resolutionForm.getName();
	        this._resolution = resolutionForm.getResolution();
	        Date startDate = resolutionForm.getStartDate();
	        this._dateCreated = startDate == null ? null : new Date(startDate.getTime());
	        this._challenge = resolutionForm.getChallenge();
	        this._amountDonated = resolutionForm.getAmountDonated();
	        this._videoUrl = resolutionForm.getVideoUrl();
	        List<String> friends = resolutionForm.getFriends();
	        this._friends = _friends == null || _friends.isEmpty() ? DEFAULT_FRIENDS : friends;

	    }
	
	 public Date getDateCreated() {
			return _dateCreated;
		}



		public void setDateCreated(Date dateCreated) {
			this._dateCreated = dateCreated;
		}



		public String getChallenge() {
			return _challenge;
		}



		public void setChallenge(String challenge) {
			this._challenge = challenge;
		}



		public Double getAmountDonated() {
			return _amountDonated;
		}



		public void setAmountDonated(Double amountDonated) {
			this._amountDonated = amountDonated;
		}



		public String getVideoUrl() {
			return _videoUrl;
		}



		public void setVideoUrl(String videoUrl) {
			this._videoUrl = videoUrl;
		}



		public String getName() {
			return _name;
		}



		public void setName(String name) {
			this._name = name;
		}



		public String getOrganizerUserId() {
			return _organizerUserId;
		}



		public void setOrganizerUserId(String organizerUserId) {
			this._organizerUserId = organizerUserId;
		}



		public String getResolution() {
			return _resolution;
		}



		public void setResolution(String resolution) {
			this._resolution = resolution;
		}



		public Key<SocialUser> getSocialUserKey() {
			return socialUserKey;
		}



		public void setSocialUserKey(Key<SocialUser> socialUserKey) {
			this.socialUserKey = socialUserKey;
		}

	    public List<String> getFriends() {
	        return _friends == null ? null : ImmutableList.copyOf(_friends);
	    }
	


}
