package com.google.devrel.training.conference.form;

import java.util.Date;
import java.util.List;

import com.google.common.collect.ImmutableList;
import com.googlecode.objectify.annotation.Ignore;

public class ChargePaymentForm {

    private boolean optOut;

    private boolean optInGiftAid;
	
    private boolean myOwnMoney;
    
    private boolean noGambling;
    
	private String title;
	
	private String firstName;

	private String lastName;

	private String address;

	private String postcode;


	private Double amountDonated;
	
	private String currency;

	private String facebookArticleID;

	private String firebaseToken;

	private String stripeToken;
    
	private String status;

	
    public ChargePaymentForm() {}


    public ChargePaymentForm(
    		boolean optOut, 
    		boolean optInGiftAid, 
    		boolean myOwnMoney, 
    		boolean noGambling, 
    		String title, 
    		String firstName, 
    		String lastName, 
    		String address, 
    		String postcode, 

    		Double amountDonated, 
    		String currency, 
    		String facebookArticleID, 
    		String firebaseToken,
    		String stripeToken,
    		String status
    		) {
    	
    	
        this.optOut = optOut;
        this.optInGiftAid = optInGiftAid;
        this.myOwnMoney = myOwnMoney;
        this.noGambling = noGambling;
        this.title = title;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.postcode = postcode;
        this.amountDonated = amountDonated;
        this.currency = currency;
        this.facebookArticleID = facebookArticleID; 
        this.firebaseToken = firebaseToken;
        this.stripeToken = stripeToken;
        this.status = status;
    }

    
    public String toString () {
    	
    	StringBuffer buffer = new StringBuffer();
    	
    	buffer.append("optOut " + this.optOut + "\n");
    	buffer.append("optInGiftAid " + this.optInGiftAid + "\n");
    	buffer.append("myOwnMoney " + this.myOwnMoney + "\n");
    	buffer.append("noGambling " + this.noGambling + "\n");
    	buffer.append("title " + this.title + "\n");
    	buffer.append("firstName " + this.firstName + "\n");
    	buffer.append("lastName " + this.lastName + "\n");
    	buffer.append("address " + this.address + "\n");
    	buffer.append("postcode " + this.postcode + "\n");
    	buffer.append("amountDonated " + this.amountDonated + "\n");
    	buffer.append("facebookArticleID " + this.facebookArticleID + "\n");
    	buffer.append("firebaseToken " + this.firebaseToken + "\n");
    	buffer.append("stripeToken " + this.stripeToken + "\n");
    	buffer.append("status " + this.status + "\n");

    	return buffer.toString();
    	
    }
    
    

	/**
	 * @return the optOut
	 */
	public boolean isOptOut() {
		return optOut;
	}


	/**
	 * @return the optInGiftAid
	 */
	public boolean isOptInGiftAid() {
		return optInGiftAid;
	}


	/**
	 * @return the myOwnMoney
	 */
	public boolean isMyOwnMoney() {
		return myOwnMoney;
	}


	/**
	 * @return the noGambling
	 */
	public boolean isNoGambling() {
		return noGambling;
	}


	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}


	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}


	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}


	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}


	/**
	 * @return the postcode
	 */
	public String getPostcode() {
		return postcode;
	}


	/**
	 * @return the amountDonated
	 */
	public Double getAmountDonated() {
		return amountDonated;
	}


	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}


	/**
	 * @return the permalink
	 */
	public String getFacebookArticleID() {
		return facebookArticleID;
	}


	/**
	 * @return the firebaseToken
	 */
	public String getFirebaseToken() {
		return firebaseToken;
	}


	/**
	 * @param optOut the optOut to set
	 */
	public void setOptOut(boolean optOut) {
		this.optOut = optOut;
	}


	/**
	 * @param optInGiftAid the optInGiftAid to set
	 */
	public void setOptInGiftAid(boolean optInGiftAid) {
		this.optInGiftAid = optInGiftAid;
	}


	/**
	 * @param myOwnMoney the myOwnMoney to set
	 */
	public void setMyOwnMoney(boolean myOwnMoney) {
		this.myOwnMoney = myOwnMoney;
	}


	/**
	 * @param noGambling the noGambling to set
	 */
	public void setNoGambling(boolean noGambling) {
		this.noGambling = noGambling;
	}


	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}


	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}


	/**
	 * @param postcode the postcode to set
	 */
	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}


	/**
	 * @param amountDonated the amountDonated to set
	 */
	public void setAmountDonated(Double amountDonated) {
		this.amountDonated = amountDonated;
	}


	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}


	/**
	 * @param permalink the permalink to set
	 */
	public void setFacebookArticleID(String facebookArticleID) {
		this.facebookArticleID = facebookArticleID;
	}


	/**
	 * @param firebaseToken the firebaseToken to set
	 */
	public void setFirebaseToken(String firebaseToken) {
		this.firebaseToken = firebaseToken;
	}


	/**
	 * @return the stripeToken
	 */
	public String getStripeToken() {
		return stripeToken;
	}


	/**
	 * @param stripeToken the stripeToken to set
	 */
	public void setStripeToken(String stripeToken) {
		this.stripeToken = stripeToken;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}
}
