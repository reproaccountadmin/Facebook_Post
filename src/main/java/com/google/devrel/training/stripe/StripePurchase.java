package com.google.devrel.training.stripe;

import java.util.logging.Logger;

import com.google.devrel.training.conference.domain.ChargePaymentDetails;

public class StripePurchase {

	final String DESCRIPTION = "New Year\'s Resolution Challenge.";
	private static final Logger LOG = Logger.getLogger(StripePurchase.class.getName());

	
	
	private String source;
	private int amount;
	private String currency;
	private String description;
	
	public StripePurchase(String source, int amount, String currency) {
		this.source = source;
		this.amount = amount;
		this.currency = currency;
		this.description =  DESCRIPTION;
	}

	public String getSource() {
		return source;
	}

	public int getAmount() {
		return amount;
	}

	public String getCurrency() {
		return currency;
	}

	public String getDescription() {
		return description;
	}


}
