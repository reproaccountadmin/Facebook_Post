package com.google.devrel.training.stripe;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import com.stripe.Stripe;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.exception.RateLimitException;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;

public class StripeCharge {

	private static final Logger LOG = Logger.getLogger(StripeCharge.class.getName());

	
	private StripeCharge() {}
	

public static Charge ChargeCard(StripePurchase stripePurchase) 
		throws CardException, 
			AuthenticationException, 
			InvalidRequestException, 
			APIConnectionException, 
			APIException,
			RateLimitException,
			StripeException,
			InvalidRequestException {
	
	Charge charge = null;
	
	Stripe.apiKey = "sk_test_YUPcQ1mspoOBKmEEztdPFlK9";
	
	Map<String, Object> chargeParams = new HashMap<String, Object>();
	chargeParams.put("amount", stripePurchase.getAmount());
	chargeParams.put("currency", stripePurchase.getCurrency());
	chargeParams.put("source", stripePurchase.getSource()); // obtained with Stripe.js
	chargeParams.put("description", stripePurchase.getDescription());

	try {
		
		charge = Charge.create(chargeParams);
			
			

		  // Use Stripe's library to make requests...
		} catch (CardException e) {
		  // Since it's a decline, CardException will be caught
		  System.out.println("Status is: " + e.getCode());
		  System.out.println("Message is: " + e.getMessage());
		
		} catch (RateLimitException e) {
		  // Too many requests made to the API too quickly
		
		}catch (AuthenticationException e) {
			  // Authentication with Stripe's API failed
			  // (maybe you changed API keys recently)
		}  catch (InvalidRequestException e) {
		  // Invalid parameters were supplied to Stripe's API
		
		} catch (APIConnectionException e) {
		  // Network communication with Stripe failed
		
		} catch (StripeException e) {
		  // Display a very generic error to the user, and maybe send
		  // yourself an email
		
		} catch (Exception e) {
		  // Something else happened, completely unrelated to Stripe
		}
	return charge;
	
}
	

}
