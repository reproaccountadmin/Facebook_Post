package com.google.devrel.training.auth;

import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

import com.google.api.server.spi.response.UnauthorizedException;
import com.google.devrel.training.conference.spi.ConferenceApi;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseToken;
import com.google.firebase.tasks.OnFailureListener;
import com.google.firebase.tasks.OnSuccessListener;
import com.google.firebase.tasks.Task;
import com.google.firebase.tasks.Tasks;
import com.google.api.server.spi.response.UnauthorizedException;

public class FirebaseAuthUtility {
    
	
	private static final Logger LOG = Logger.getLogger(FirebaseAuthUtility.class.getName());


	private static Task<FirebaseToken> AuthTask(String token) {
	
	Task<FirebaseToken> authTask = FirebaseAuth.getInstance().verifyIdToken(token).addOnSuccessListener(new OnSuccessListener<FirebaseToken>() {
		
			@Override
			public void onSuccess(FirebaseToken decodedToken) {
				String uid = decodedToken.getUid();
				LOG.info("User ID " + uid);
			}
	
		}).addOnFailureListener(new OnFailureListener() {
			@Override
			public void onFailure(Exception excptn) {
				LOG.info("Failure " + excptn.getMessage());
	
			}
		});

	
	return authTask;
	}
	
	public static FirebaseToken FirebaseToken(String token) throws IllegalArgumentException {
	
		Task<FirebaseToken> authTask = FirebaseAuthUtility.AuthTask(token);
		
		try {
			Tasks.await(authTask);
		} catch (ExecutionException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	    if(!authTask.isSuccessful()) {
	    	throw new IllegalArgumentException("Firebase Authorization required - Error 100");
	    }
	    
	    
	    return authTask.getResult();
	}
	
			
	
}
