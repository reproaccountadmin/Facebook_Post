package com.google.devrel.training.auth;

import java.util.Map;

import com.google.firebase.auth.FirebaseToken;

public class AppengineFirebaseToken {

	Map<String, Object> claims;
	
    private String uid;

    private String issuer;

    private String name;

    private String picture;

    private String email;

    private boolean emailVerified;

    public AppengineFirebaseToken(FirebaseToken token)  {

    	claims = token.getClaims();
    	email = token.getEmail();
    	issuer = token.getIssuer();
    	name = token.getName();
    	picture = token.getPicture();
    	uid = token.getUid();
    	
    }

    public String getUid() {
        return uid;
    }

    public String getIssuer() {
        return issuer;
    }

    public String getName() {
        return name;
    }

    public String getPicture() {
        return picture;
    }

    public String getEmail() {
        return email;
    }

    public boolean isEmailVerified() {
        return Boolean.valueOf((String) claims.get("email_verified"));
    }

}
